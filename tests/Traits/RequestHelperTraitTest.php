<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Traits;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\RequestHelperTrait
 */
class RequestHelperTraitTest extends TestCase
{
    use ProphecyTrait;

    public function testFake(): void
    {
        $this->markTestIncomplete('Not a real test...');
    }
}
