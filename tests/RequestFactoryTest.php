<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests;

use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\MultipartStream;
use Neclimdul\OpenapiPhp\Helper\RequestFactory;
use Neclimdul\OpenapiPhp\Helper\Serialization\Serializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Http\Message\RequestInterface;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\RequestFactory
 */
class RequestFactoryTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @var \Neclimdul\OpenapiPhp\Helper\RequestFactory<MockModel>
     */
    private RequestFactory $sot;

    public function setUp(): void
    {
        parent::setUp();
        $this->sot = new RequestFactory(
            new HttpFactory(),
            new Serializer(MockModel::class),
        );
    }

    public static function provideMethods(): \Generator
    {
        $methods = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'];
        foreach ($methods as $method) {
            yield [$method];
        }
    }

    /**
     * @covers ::createRequest
     * @dataProvider provideMethods
     */
    public function testCreateRequestMethods(string $method): void
    {
        $r = $this->sot->createRequest($method, '/test', [], [], [], '');
        $this->assertInstanceOf(RequestInterface::class, $r);
        $this->assertEquals($method, $r->getMethod());
    }

    /**
     * @return array{
     *    string,
     *     1?: string,
     *     2?: array<string, string|string[]>
     * }[]
     */
    public static function provideUris(): array
    {
        return [
            ['/test'],
            ['/%20fooo%20bar', '/ fooo bar'],
            ['/test?foo=bar&biz=baz&biz=bez', '/test', ['foo' => 'bar', 'biz' => ['baz', 'bez']]],
        ];
    }

    /**
     * @param string $expected
     *   Expected request URI.
     * @param string|null $uri
     *   Base URI.
     * @param array<string|string[]>|null $query
     *   Additional URI query arguments.
     * @covers ::createRequest
     * @dataProvider provideUris
     */
    public function testCreateRequestUri(string $expected, ?string $uri = null, ?array $query = null): void
    {
        $uri ??= $expected;
        $query ??= [];
        $r = $this->sot->createRequest('GET', $uri, $query, [], [], '');
        $this->assertEquals($expected, (string)$r->getUri());
    }

    /**
     * @return array{
     *    string,
     *     mixed,
     *     array<string, string>
     * }[]
     */
    public static function provideBodies(): array
    {
        return [
            'empty' => [
                '',
                '',
                ['Content-Type' => 'application/json'],
            ],
            'string' => [
                '"test string"',
                'test string',
                ['Content-Type' => 'application/json'],
            ],
            'array' => [
                '["test string","something"]',
                ['test string', 'something'],
                ['Content-Type' => 'application/json'],
            ],
            'object' => [
                '{}',
                new \stdClass(),
                ['Content-Type' => 'application/json'],
            ],
            // TODO Headers
            // TODO Models
            // TODO Files
        ];
    }

    /**
     * @param string $expected
     * @param mixed $body
     * @param array<string, string> $headers
     *
     * @covers ::createRequest
     * @covers ::createMultipartStream
     * @dataProvider provideBodies
     */
    public function testCreateRequestBody(string $expected, mixed $body, array $headers): void
    {
        $r = $this->sot->createRequest('GET', '/test', [], $headers, [], $body);
        $this->assertEquals($expected, (string)$r->getBody());
    }

    /**
     * @covers ::createRequest
     */
    public function testCreateRequestFormBody(): void
    {
        $this->expectException(\AssertionError::class);
        $this->sot->createRequest('GET', '/test', [], [], ['foo' => 'bar'], 'test string');
    }

    /**
     * @return array{
     *     string,
     *     array<string|resource[]|scalar[]|null>,
     *     2?: string[]|null
     * }[]
     */
    public static function provideForms(): array
    {
        return [
            ['', []],
            // ['', ['foo' => 'bar']],
            // JSON form data doesn't make a lot of sense...
            // ['', ['foo' => 'bar'], ''],
            ['', [], ['Content-Type' => 'application/x-www-form-urlencoded']],
            [
                'foo=bar&biz=1&biz=2&biz=3',
                ['foo' => 'bar', 'biz' => [1, 2, 3]],
                ['Content-Type' => 'application/x-www-form-urlencoded']
            ],
        ];
    }

    /**
     * @param string $expected
     *   Expected request body.
     * @param array<string|resource[]|scalar[]|null> $form
     *   Form parameters.
     * @param string[] $headers
     *   List of headers
     *
     * @covers ::createRequest
     * @dataProvider provideForms
     */
    public function testCreateRequestForm($expected, $form, $headers = ['Content-Type' => 'application/json']): void
    {
        $r = $this->sot->createRequest('GET', '/test', [], $headers, $form, '');
        $this->assertEquals($expected, (string)$r->getBody());
    }

    /**
     * @covers ::createRequest
     * @covers ::createMultipartStream
     */
    public function testCreateRequestForm2(): void
    {
        $form = ['foo' => 'bar', 'biz' => [1, 2, 3]];
        $headers = ['Content-Type' => 'multipart/form-data'];

        $r = $this->sot->createRequest('GET', '/test', [], $headers, $form, '');
        $b = $r->getBody();
        $this->assertInstanceOf(MultipartStream::class, $b);
        $this->assertEquals(
            (string)(new MultipartStream([
                [
                    'name' => 'foo',
                    'contents' => 'bar',
                ],
                [
                    'name' => 'biz',
                    'contents' => 1,
                ],
                [
                    'name' => 'biz',
                    'contents' => 2,
                ],
                [
                    'name' => 'biz',
                    'contents' => 3,
                ],
            ], $b->getBoundary())),
            (string)$b
        );

        $root = vfsStream::setup('test');
        $file = vfsStream::newFile('test.txt')->at($root);
        file_put_contents($file->url(), 'test value');
        $file_reference = fopen($file->url(), 'r');
        $form['file'] = [fopen($file->url(), 'r')];
        $headers = ['Content-Type' => 'application/x-www-form-urlencoded'];
        $r = $this->sot->createRequest('GET', '/test', [], $headers, $form, '');
        $b = $r->getBody();
        $this->assertInstanceOf(MultipartStream::class, $b);
        $this->assertEquals(
            (string)(new MultipartStream([
                [
                    'name' => 'foo',
                    'contents' => 'bar',
                ],
                [
                    'name' => 'biz',
                    'contents' => 1,
                ],
                [
                    'name' => 'biz',
                    'contents' => 2,
                ],
                [
                    'name' => 'biz',
                    'contents' => 3,
                ],
                [
                    'name' => 'file',
                    'contents' => $file_reference,
                ],
            ], $b->getBoundary())),
            (string)$b
        );
    }
}
