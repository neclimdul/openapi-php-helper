<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests\Response;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseSync
 */
class ApiResponseSyncTest extends ApiResponseTest
{
    /**
     * @covers ::__construct
     * @covers ::getResponse
     */
    public function testGetResponse(): void
    {
        $this->assertSame($this->response, $this->sot->getResponse());
    }
}
