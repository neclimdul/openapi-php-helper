<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests\Response;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseSync;
use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Response\ApiResponse
 */
class ApiResponseTest extends TestCase
{
    protected RequestInterface $request;
    protected ResponseInterface $response;
    protected ApiResponseInterface $sot;
    private Deserializer $deserializer;
    private ResponseTypeMap $responseMap;

    public function setUp(): void
    {
        parent::setUp();
        $this->request = new Request('GET', '/');
        $this->response = new Response(
            200,
            ['Content-Type' => ['application-json']],
            '{"testString": "foobar"}',
        );
        $this->response->getStatusCode();
        $this->deserializer = new Deserializer();
        $this->responseMap = new ResponseTypeMap(
            [
                '200' => ['application-json' => ['type' => MockModel::class]],
                '204' => ['application-json' => ['type' => null]],
            ],
        );
        $this->sot = new ApiResponseSync(
            $this->request,
            $this->response,
            $this->deserializer,
            $this->responseMap,
        );
    }

    /**
     * @covers ::__construct
     * @covers ::getRequest
     */
    public function testGetRequest(): void
    {
        $this->assertSame($this->request, $this->sot->getRequest());
    }

    /**
     * @covers ::__construct
     * @covers ::getData
     */
    public function testGetData(): void
    {
        $data = $this->sot->getData();
        $this->assertInstanceOf(
            MockModel::class,
            $data,
        );
        $this->assertEquals(
            'foobar',
            $data->getTestString(),
        );
        $this->assertNull(
            (new ApiResponseSync(
                $this->request,
                $this->response->withStatus(204),
                $this->deserializer,
                $this->responseMap,
            ))->getData()
        );
    }

    /**
     * @covers ::toArray
     */
    public function testResponseToReturn(): void
    {
        [$data, $code, $headers] = $this->sot->toArray();
        $this->assertInstanceOf(
            MockModel::class,
            $data,
        );
        $this->assertEquals(
            'foobar',
            $data->getTestString(),
        );
        $this->assertEquals(
            200,
            $code,
        );
        $this->assertEquals(
            $this->response->getHeaders(),
            $headers,
        );
    }

    /**
     * @return array<array{int, bool}>
     */
    public static function provideStatusCodes(): array
    {
        return [
            [200, true],
            [201, true],
            [204, true],
            [300, false],
            [301, false],
            [302, false],
            [400, false],
            [401, false],
            [404, false],
            [500, false],
            [505, false],
        ];
    }

    /**
     * @covers ::isSuccess
     * @dataProvider provideStatusCodes
     */
    public function testIsSuccess(int $code, bool $expected): void
    {
        $sot = new ApiResponseSync(
            $this->request,
            $this->response->withStatus($code),
            $this->deserializer,
            $this->responseMap,
        );
        $this->assertEquals($expected, $sot->isSuccess());
    }
}
