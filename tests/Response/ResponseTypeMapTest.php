<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Response;

use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap
 */
final class ResponseTypeMapTest extends TestCase
{
    private ResponseTypeMap $default_sot;
    private ResponseTypeMap $simple_sot;

    /** @var array<array-key,array<string,array{type: class-string|string|null}>> */
    private static array $default_schema = [
        '200' => [
            'application/json' => ['type' => 'string'],
        ],
        'default' => [
            '*/*' => ['type' => MockModelAdditional::class],
            'application/json' => ['type' => MockModel::class],
        ],
    ];

    /** @var array<array-key,array<string,array{type: class-string|string|null}>> */
    private static array $simple_schema = [
        '200' => [
            'application/json' => ['type' => 'string'],
        ],
        '400' => [],
        '401' => [
            'application/json' => ['type' => null],
        ],
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->default_sot = new ResponseTypeMap(static::$default_schema);
        $this->simple_sot = new ResponseTypeMap(static::$simple_schema);
    }

    public static function provideSchema(): \Generator
    {
        foreach (self::$default_schema as $code => $data) {
            foreach ($data as $content_type => $definition) {
                yield [$code, $content_type, $definition];
            }
        }
    }

    /**
     * @covers ::__construct
     * @covers ::getSchema
     * @dataProvider provideSchema
     * @param int|string $code
     * @param string $content_type
     * @param array{type: class-string|string|null} $definition
     */
    public function testGetSchema(int|string $code, string $content_type, array $definition): void
    {
        $this->assertEquals(
            $definition,
            $this->default_sot->getSchema((string)$code, $content_type),
        );
    }

    /**
     * @covers ::getSchema
     */
    public function testGetSchemaFallback(): void
    {
        $this->assertEquals(
            static::$default_schema['default']['*/*'],
            $this->default_sot->getSchema('default', 'dne'),
        );
    }

    /**
     * @covers ::setSchema
     */
    public function testSetSchema(): void
    {
        $this->default_sot->setSchema('418', 'teapot', ['type' => 'string']);
        $this->assertEquals(
            ['type' => 'string'],
            $this->default_sot->getSchema('418', 'teapot'),
        );
    }

    /**
     * @covers ::getResponseCodes
     */
    public function testGetResponseCodes(): void
    {
        $this->assertEquals(
            ['200', 'default'],
            $this->default_sot->getResponseCodes(),
        );
    }

    /**
     * @covers ::getContentTypes
     */
    public function testGetContentTypes(): void
    {
        $this->assertEquals(
            ['application/json'],
            $this->default_sot->getContentTypes('200'),
        );
        $this->assertEquals(
            ['*/*', 'application/json'],
            $this->default_sot->getContentTypes('default'),
        );
        $this->assertEquals(
            [],
            $this->default_sot->getContentTypes('418'),
        );
    }

    /**
     * @covers ::getResponseType
     * @dataProvider provideSchema
     * @param int|string $code
     * @param string $content_type
     * @param array{type: class-string|string|null} $definition
     */
    public function testGetResponseType(int|string $code, string $content_type, array $definition): void
    {
        $this->assertEquals(
            $definition['type'],
            $this->default_sot->getResponseType($code, $content_type),
        );
    }

    /**
     * @covers ::getResponseType
     */
    public function testComplexContentType(): void
    {
        $this->assertEquals(
            static::$simple_schema['200']['application/json']['type'],
            $this->simple_sot->getResponseType('200', 'application/json;charset=utf-8'),
        );
    }

    /**
     * @covers ::getResponseType
     */
    public function testGetResponseTypeFallback(): void
    {
        $this->assertEquals(
            static::$default_schema['default']['*/*']['type'],
            $this->default_sot->getResponseType('default', 'dne'),
        );
        $this->assertEquals(
            static::$default_schema['default']['*/*']['type'],
            $this->default_sot->getResponseType('418', 'dne'),
        );
        $this->assertEquals(
            static::$default_schema['default']['*/*']['type'],
            $this->default_sot->getResponseType('200', 'dne'),
        );

        $this->assertEquals(
            null,
            $this->simple_sot->getResponseType('400', 'dne'),
        );
        $this->assertEquals(
            null,
            $this->simple_sot->getResponseType('401', 'dne'),
        );
        $this->assertEquals(
            null,
            $this->simple_sot->getResponseType('299', 'dne'),
        );
    }
}
