<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests\Response;

use GuzzleHttp\Promise\FulfilledPromise;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseAsync;
use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseAsync
 * @property \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseAsync $sot
 */
class ApiResponseAsyncTest extends ApiResponseTest
{
    private FulfilledPromise $promise;

    public function setUp(): void
    {
        parent::setUp();
        $this->promise = new FulfilledPromise($this->response);
        $this->sot = new ApiResponseAsync(
            $this->request,
            $this->promise,
            new Deserializer(),
            new ResponseTypeMap(['200' => ['application-json' => ['type' => MockModel::class]]]),
        );
    }

    /**
     * @covers ::__construct
     * @covers ::getResponse
     */
    public function testGetResponse(): void
    {
        $this->assertSame($this->response, $this->sot->getResponse());
    }

    /**
     * @covers ::__construct
     * @covers ::getPromise
     */
    public function testGetPromise(): void
    {
        $this->assertSame($this->promise, $this->sot->getPromise());
    }
}
