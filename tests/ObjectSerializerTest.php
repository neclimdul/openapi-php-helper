<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests;

use GuzzleHttp\Psr7\Utils;
use Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface as LegacyApiExceptionInterface;
use Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Neclimdul\OpenapiPhp\Helper\ObjectSerializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\ApiException;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional;
use Neclimdul\OpenapiPhp\Helper\Tests\Serialization\DeserializerTest;
use Neclimdul\OpenapiPhp\Helper\Tests\Serialization\SerializerTest;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\ObjectSerializer
 */
class ObjectSerializerTest extends TestCase
{
    /**
     * @var \Neclimdul\OpenapiPhp\Helper\ObjectSerializer<
     *   \Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\ApiException,
     *   \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
     *   >
     */
    private ObjectSerializer $sot;

    protected function setUp(): void
    {
        parent::setUp();
        $this->sot = new ObjectSerializer(
            ModelInterface::class,
            ApiException::class
        );
    }

    /**
     * @covers ::processQuery
     */
    public function testProcessQuery(): void
    {
        $this->markTestIncomplete('still needed?');
    }

    /**
     * @covers ::fileToFormValue
     */
    public function testFileToFormValue(): void
    {
        $this->markTestIncomplete('still needed?');
    }

    /**
     * @covers ::sanitizeForSerialization
     */
    public function testSanitizeForSerialization(): void
    {
        $this->assertEquals(true, $this->sot->sanitizeForSerialization(true));
        $this->assertEquals('true', $this->sot->sanitizeForSerialization('true'));
        $this->assertEquals(123, $this->sot->sanitizeForSerialization(123));
        $this->assertEquals(123.23, $this->sot->sanitizeForSerialization(123.23));
        $scalar_array = [
            true,
            false,
            null,
            'true',
            'this is a string',
            123,
            123.23,
        ];
        $this->assertEquals($scalar_array, $this->sot->sanitizeForSerialization($scalar_array));
        $this->assertEquals((object)$scalar_array, $this->sot->sanitizeForSerialization((object)$scalar_array));

        // Dates.
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        $dt = '2020-09-08T09:51:12+05:00';
        $this->assertEquals('2020-09-08', $this->sot->sanitizeForSerialization($date, null, 'date'));
        $this->assertEquals($dt, $this->sot->sanitizeForSerialization($date));
        $this->assertEquals(['date' => $dt], $this->sot->sanitizeForSerialization(['date' => $date]));
        $this->assertEquals((object)['date' => $dt], $this->sot->sanitizeForSerialization((object)['date' => $date]));
    }

    /**
     * @return array<array-key, array{class-string, array<mixed>, object}>
     */
    public static function provideBodyValues(): array
    {
        return SerializerTest::provideBodyValues();
    }

    /**
     * @covers ::sanitizeForSerialization
     * @dataProvider provideBodyValues
     * @param class-string $model_class
     * @param array<array-key, array{class-string, array<mixed>, object}> $data
     */
    public function testSanitizeForSerializationModels(string $model_class, array $data, object $expected): void
    {
        $model = new $model_class($data);
        $this->assertEquals($expected, $this->sot->sanitizeForSerialization($model));
    }

    /**
     * @return array<array-key, array{string, string}>
     */
    public static function provideFileNames(): array
    {
        return SerializerTest::provideFileNames();
    }

    /**
     * @covers ::sanitizeFilename
     * @dataProvider provideFileNames
     */
    public function testSanitizeFilename(string $expected, string $input): void
    {
        $this->assertEquals($expected, $this->sot->sanitizeFilename($input));
    }

    /**
     * @return array<string, array{string, mixed}>
     */
    public static function providePathValues(): array
    {
        return SerializerTest::providePathValues();
    }

    /**
     * @param mixed $input
     * @covers ::toPathValue
     * @dataProvider providePathValues
     */
    public function testToPathValue(string $expected, mixed $input): void
    {
        $this->assertEquals($expected, $this->sot->toPathValue($input));
    }

    /**
     * @param mixed $input
     * @covers ::toQueryValue
     * @dataProvider providePathValues
     */
    public function testToQueryValue(string $expected, mixed $input): void
    {
        // $this->assertEquals($expected, $this->sot->toQueryValue($input));
        $this->markTestIncomplete('TODO');
    }

    /**
     * @covers ::toBodyValue
     * @dataProvider provideBodyValues
     * @param class-string $model_class
     * @param array<array-key, array{class-string, array<mixed>, object}> $data
     */
    public function testToBodyValue(string $model_class, array $data, object $expected): void
    {
        $this->assertEquals(
            json_encode($expected),
            $this->sot->toBodyValue(new $model_class($data), 'application/json')
        );
    }

    /**
     * @covers ::toBodyValue
     */
    public function testToBodyValueEdgeCases(): void
    {
        $this->assertEquals('test', $this->sot->toBodyValue('test', 'test/type'));
        $filename = __DIR__ . '/Fixtures/MockModel.php';
        $f = fopen($filename, 'r');
        $stream = Utils::streamFor($f);
        $this->assertEquals(file_get_contents($filename), $this->sot->toBodyValue($stream, 'test/type'));
        // Broken?
        $this->assertEquals('', $this->sot->toBodyValue([], 'test/type'));
    }

    /**
     * Serialization test cases.
     *
     * @see https://swagger.io/specification/#style-examples
     *
     * @return array<array{string, string|scalar[], string}>
     */
    public static function provideSerializationValues(): array
    {
        return SerializerTest::provideCollectionSerializationValues();
    }

    /**
     * Test basic serialization.
     *
     * @param string $expected
     *   Expected serialized value.
     * @param string|scalar[] $collection
     *   Test collection.
     * @param string $style
     *   Serialization style.
     *
     * @covers ::serializeCollection
     * @dataProvider provideSerializationValues
     */
    public function testSerializeCollection(string $expected, $collection, string $style): void
    {
        $this->assertEquals($expected, $this->sot->serializeCollection($collection, $style, true));
    }

    /**
     * @covers ::toHeaderValue
     */
    public function testToHeaderValue(): void
    {
        $this->markTestIncomplete('TODO');
    }

    /**
     * @return array<string, array{mixed, string|null, string}>
     */
    public static function provideDeserialize(): array
    {
        return DeserializerTest::provideDeserialize();
    }

    /**
     * @return array<string, array{class-string, string, string}>
     */
    public static function provideDeserializeErrors(): array
    {
        return [
            'bad object data' => [\InvalidArgumentException::class, '', MockModel::class],
            'bad array data' => [\InvalidArgumentException::class, '', MockModel::class . '[]'],
            'bad json' => [ApiException::class, 'bad{}json', MockModel::class . '[]'],
            'bad xml' => [ApiException::class, 'bad{}xml', MockModel::class . '[]', 'application/xml'],
        ];
    }

    /**
     * @param mixed $expected
     * @param class-string $type
     * @covers ::deserialize
     * @dataProvider provideDeserialize
     */
    public function testDeserialize(
        mixed $expected,
        ?string $value,
        string $type,
        string $contentType = 'application/json'
    ): void {
        $this->assertEquals($expected, $this->sot->deserialize($value, $type, ['Content-Type' => [$contentType]]));
    }

    /**
     * @covers ::deserialize
     */
    public function testDeserializeFile(): void
    {
        $filename = __DIR__ . '/Fixtures/MockModel.php';
        $f = fopen($filename, 'r');
        $stream = Utils::streamFor($f);
        $t = $this->sot->deserialize($stream, \SplFileObject::class);
        $this->assertInstanceOf(\SplFileObject::class, $t);
        $t->rewind();
        $this->assertEquals((string)$stream, $t->fread((int) filesize($filename)));

        $t = $this->sot->deserialize($stream, '\SplFileObject');
        $this->assertInstanceOf(\SplFileObject::class, $t);
        $t->rewind();
        $this->assertEquals((string)$stream, $t->fread((int) filesize($filename)));

        $root = vfsStream::setup('test');
        $filename = $root->url() . '/foo.txt';
        file_put_contents($filename, 'test data');
        $f = fopen($filename, 'r');
        $stream = Utils::streamFor($f);
        $t = $this->sot->deserialize($stream, \SplFileObject::class);
        $this->assertInstanceOf(\SplFileObject::class, $t);
        $t->rewind();
        $this->assertEquals((string)$stream, $t->fread((int) filesize($filename)));
    }

    /**
     * @param class-string<\Throwable> $expected
     * @param class-string $type
     * @covers ::deserialize
     * @dataProvider provideDeserializeErrors
     */
    public function testDeserializeErrors(
        $expected,
        string $value,
        $type,
        string $contentType = 'application/json'
    ): void {
        $this->expectException($expected);
        try {
            $this->sot->deserialize($value, $type, ['Content-Type' => [$contentType]]);
        } catch (\Throwable $exception) {
            if ($exception instanceof LegacyApiExceptionInterface) {
                $this->assertEquals($value, $exception->getResponseBody());
            }
            throw $exception;
        }
    }

    /**
     * @covers ::setDateTimeFormat
     */
    public function testSetDateTimeFormat(): void
    {
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        $dt = 'Tuesday, 08-Sep-2020 09:51:12 GMT+0500';
        $this->sot->setDateTimeFormat(\DateTimeInterface::COOKIE);
        $this->assertEquals('2020-09-08', $this->sot->sanitizeForSerialization($date, null, 'date'));
        $this->assertEquals($dt, $this->sot->sanitizeForSerialization($date));
        $this->assertEquals([$dt], $this->sot->sanitizeForSerialization([$date]));
        $this->assertEquals((object)['date' => $dt], $this->sot->sanitizeForSerialization((object)['date' => $date]));
    }

    /**
     * @covers ::toFormValue
     */
    public function testToFormValue(): void
    {
        $this->markTestIncomplete('TODO');
    }

    /**
     * @covers ::toString
     */
    public function testToString(): void
    {
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        $dt = '2020-09-08T09:51:12+05:00';

        $this->assertEquals('true', $this->sot->toString(true));
        $this->assertEquals('false', $this->sot->toString(false));
        $this->assertEquals('string', $this->sot->toString('string'));
        $this->assertEquals('123', $this->sot->toString(123));
        $this->assertEquals($dt, $this->sot->toString($date));

        $dt = 'Tuesday, 08-Sep-2020 09:51:12 GMT+0500';
        $this->sot->setDateTimeFormat(\DateTimeInterface::COOKIE);
        $this->assertEquals($dt, $this->sot->toString($date));
    }
}
