<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests;

use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\Configuration;
use PHPUnit\Framework\TestCase;

/**
 *
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Configuration
 */
class ConfigurationTest extends TestCase
{
    private Configuration $sot;

    protected function setUp(): void
    {
        parent::setUp();
        $this->sot = new Configuration('http://example.com:123/', 'asdf123');
    }

    /**
     * @covers ::__construct
     * @covers ::setHost
     * @covers ::getHost
     */
    public function testGetHost(): void
    {
        $this->assertEquals('http://example.com:123/', $this->sot->getHost());
        $this->assertSame($this->sot, $this->sot->setHost('http://example.com:1234/'));
        $this->assertEquals('http://example.com:1234/', $this->sot->getHost());
    }

    /**
     * @covers ::__construct
     * @covers ::setUserAgent
     * @covers ::getUserAgent
     */
    public function testGetUserAgent(): void
    {
        $this->assertEquals('asdf123', $this->sot->getUserAgent());
        $this->assertSame($this->sot, $this->sot->setUserAgent('test_value'));
        $this->assertEquals('test_value', $this->sot->getUserAgent());
    }

    /**
     * @covers ::setApiKey
     * @covers ::getApiKey
     */
    public function testGetApiKey(): void
    {
        $this->assertNull($this->sot->getApiKey('test'));
        $this->assertSame($this->sot, $this->sot->setApiKey('test', 'test_value'));
        $this->assertEquals('test_value', $this->sot->getApiKey('test'));
        $this->assertNull($this->sot->getApiKey('test2'));
    }

    /**
     * @covers ::setApiKeyPrefix
     * @covers ::getApiKeyPrefix
     */
    public function testGetApiKeyPrefix(): void
    {
        $this->assertNull($this->sot->getApiKeyPrefix('test'));
        $this->assertSame($this->sot, $this->sot->setApiKeyPrefix('test', 'test_value'));
        $this->assertEquals('test_value', $this->sot->getApiKeyPrefix('test'));
        $this->assertNull($this->sot->getApiKeyPrefix('test2'));
    }

    /**
     * @covers ::setApiKey
     * @covers ::setApiKeyPrefix
     * @covers ::getApiKeyWithPrefix
     */
    public function testGetApiKeyWithPrefix(): void
    {
        $this->assertNull($this->sot->getApiKeyWithPrefix('test'));
        $this->sot->setApiKey('test', 'test_value');
        $this->assertEquals('test_value', $this->sot->getApiKeyWithPrefix('test'));
        $this->sot->setApiKeyPrefix('test', 'Bearer:');
        $this->assertEquals('Bearer: test_value', $this->sot->getApiKeyWithPrefix('test'));
        $this->assertNull($this->sot->getApiKeyWithPrefix('test2'));
    }

    /**
     * @covers ::setAccessToken
     * @covers ::getAccessToken
     */
    public function testGetAccessToken(): void
    {
        $this->assertEquals('', $this->sot->getAccessToken());
        $this->assertSame($this->sot, $this->sot->setAccessToken('test_value'));
        $this->assertEquals('test_value', $this->sot->getAccessToken());
    }

    /**
     * @covers ::setUsername
     * @covers ::getUsername
     */
    public function testGetUsername(): void
    {
        $this->assertEquals('', $this->sot->getUsername());
        $this->assertSame($this->sot, $this->sot->setUsername('test_value'));
        $this->assertEquals('test_value', $this->sot->getUsername());
    }

    /**
     * @covers ::setPassword
     * @covers ::getPassword
     */
    public function testGetPassword(): void
    {
        $this->assertEquals('', $this->sot->getPassword());
        $this->assertSame($this->sot, $this->sot->setPassword('test_value'));
        $this->assertEquals('test_value', $this->sot->getPassword());
    }

    /**
     * @covers ::setDebug
     * @covers ::getDebug
     */
    public function testGetDebug(): void
    {
        $this->assertFalse($this->sot->getDebug());
        $this->assertSame($this->sot, $this->sot->setDebug(true));
        $this->assertTrue($this->sot->getDebug());
        $this->assertSame($this->sot, $this->sot->setDebug(false));
        $this->assertFalse($this->sot->getDebug());
    }

    /**
     * @covers ::setDebugFile
     * @covers ::getDebugFile
     */
    public function testGetDebugFile(): void
    {
        $this->assertEquals('php://output', $this->sot->getDebugFile());
        $this->assertSame($this->sot, $this->sot->setDebugFile('test_value'));
        $this->assertEquals('test_value', $this->sot->getDebugFile());
    }

    /**
     * @covers ::__construct
     * @covers ::setTempFolderPath
     * @covers ::getTempFolderPath
     */
    public function testGetTempFolderPath(): void
    {
        $this->assertEquals(sys_get_temp_dir(), $this->sot->getTempFolderPath());
        $this->assertSame($this->sot, $this->sot->setTempFolderPath('test_value'));
        $this->assertEquals('test_value', $this->sot->getTempFolderPath());
    }


    /**
     * @covers ::__construct
     * @covers ::getHostFromSettings
     * @dataProvider provideInvalidHostIndexes
     */
    public function testGetHostFromSettings(): void
    {
        $this->assertEquals(
            'https://api.example.com',
            $this->sot->getHostFromSettings(0)
        );

        $this->assertEquals(
            'https://123.api.example.com/1/',
            $this->sot->getHostFromSettings(1)
        );
        $this->assertEquals(
            'https://234.api.example.com/2/',
            $this->sot->getHostFromSettings(1, ['id' => '234', 'version' => 2])
        );
    }

    /**
     * @return array<string, array{int, array<string, mixed>}>
     */
    public static function provideInvalidHostIndexes(): array
    {
        return [
            'index out of bounds positive' => [3, []],
            'index out of bounds negative' => [-4, []],
            'invalid enum' => [1, ['version' => 3]]
        ];
    }

    /**
     * @covers ::__construct
     * @covers ::getHostFromSettings
     * @param array<string, scalar>|null $variables hash of variable and the corresponding value (optional)
     * @dataProvider provideInvalidHostIndexes
     */
    public function testGetHostFromSettingsInvalid(int $index, ?array $variables): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->sot->getHostFromSettings($index, $variables);
    }
}
