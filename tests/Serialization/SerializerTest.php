<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests\Serialization;

use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Neclimdul\OpenapiPhp\Helper\Serialization\Serializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Serialization\Serializer
 */
class SerializerTest extends TestCase
{
    /** @var \Neclimdul\OpenapiPhp\Helper\Serialization\Serializer<ModelInterface> */
    private Serializer $sot;

    public function setUp(): void
    {
        parent::setUp();
        $this->sot = new Serializer(ModelInterface::class);
    }

    /**
     * Serialization test cases.
     *
     * @see https://swagger.io/specification/#style-examples
     *
     * @return array<array{string, string|scalar[], string}>
     */
    public static function provideCollectionSerializationValues(): array
    {
        $empty = [];
        $string = 'blue';
        $array = ['blue', 'black', 'brown'];
        $object = ['R' => 100, 'G' => 200, 'B' => 150];
        return [
//            'matrix empty array' => [
//                ';color',
//                $empty_array,
//                'matrix',
//            ],
//            'matrix empty string' => [
//                ';color',
//                $empty_string,
//                'matrix',
//            ],
//            'matrix empty null' => [
//                ';color',
//                $empty_null,
//                'matrix',
//            ],
//            'matrix string' => [
//                ';color=blue',
//                $string,
//                'matrix',
//            ],
            // TODO finish with matrix test cases
            // TODO label test cases
            // TODO form test cases
            'simple empty' => [
                '',
                $empty,
                'simple',
            ],
            'simple string' => [
                'blue',
                $string,
                'simple',
            ],
            'simple array' => [
                'blue,black,brown',
                $array,
                'simple',
            ],
            // TODO This is currently broken according to the spec.
            // 'simple object' => [
            //     'R,100,G,200,B,150',
            //     $object,
            //     'simple',
            // ],
            'csv empty' => [
                '',
                $empty,
                'csv',
            ],
            'csv string' => [
                'blue',
                $string,
                'csv',
            ],
            'csv array' => [
                'blue,black,brown',
                $array,
                'csv',
            ],
            // TODO This is currently broken according to the spec.
            // 'csv object' => [
            //     'R,100,G,200,B,150',
            //     $object,
            //     'csv',
            // ],
            'ssv empty' => [
                '',
                $empty,
                'ssv',
            ],
            'ssv string' => [
                'blue',
                $string,
                'ssv',
            ],
            'ssv array' => [
                'blue black brown',
                $array,
                'ssv',
            ],
            // TODO This is currently broken according to the spec.
            // 'ssv object' => [
            //     'R,100,G,200,B,150',
            //     $object,
            //     'ssv',
            // ],
            'spaceDelimited empty' => [
                '',
                $empty,
                'spaceDelimited',
            ],
            'spaceDelimited string' => [
                'blue',
                $string,
                'spaceDelimited',
            ],
            'spaceDelimited array' => [
                'blue black brown',
                $array,
                'spaceDelimited',
            ],
            // TODO This is currently broken according to the spec.
            // 'spaceDelimited object' => [
            //     'R,100,G,200,B,150',
            //     $object,
            //     'spaceDelimited',
            // ],
            'pipes empty' => [
                '',
                $empty,
                'pipes',
            ],
            'pipes string' => [
                'blue',
                $string,
                'pipes',
            ],
            'pipes array' => [
                'blue|black|brown',
                $array,
                'pipes',
            ],
            // TODO This is currently broken according to the spec.
            // 'pipes object' => [
            //     'R,100,G,200,B,150',
            //     $object,
            //     'pipes',
            // ],
            'pipeDelimited empty' => [
                '',
                $empty,
                'pipeDelimited',
            ],
            'pipeDelimited string' => [
                'blue',
                $string,
                'pipeDelimited',
            ],
            'pipeDelimited array' => [
                'blue|black|brown',
                $array,
                'pipeDelimited',
            ],
            // TODO This is currently broken according to the spec.
            // 'pipeDelimited object' => [
            //     'R,100,G,200,B,150',
            //     $object,
            //     'pipeDelimited',
            // ],
            'tsv' => [
                "blue\tblack\tbrown",
                $array,
                'tsv',
            ],
            'multi empty' => [
                '',
                $empty,
                'multi',
            ],
            'multi string' => [
                'blue',
                $string,
                'multi',
            ],
            'multi array' => [
                '0=blue&1=black&2=brown',
                $array,
                'multi',
            ],
            'multi object' => [
                'R=100&G=200&B=150',
                $object,
                'multi',
            ],
        ];
    }

    /**
     * Test basic serialization.
     *
     * @param string $expected
     *    Expected serialized value.
     * @param string|scalar[] $collection
     *    Test collection.
     * @param string $style
     *    Serialization style.
     *
     * @covers ::serializeCollection
     * @dataProvider provideCollectionSerializationValues
     */
    public function testSerializeCollection(string $expected, $collection, string $style): void
    {
        $this->assertEquals($expected, $this->sot->serializeCollection($collection, $style, true));
    }

    /**
     * @covers ::fileToFormValue
     */
    public function testFileToFormValue(): void
    {
        $this->markTestIncomplete('still needed?');
    }

    /**
     * @covers ::toFormValue
     */
    public function testToFormValue(): void
    {
        $this->markTestIncomplete('TODO');
    }

    /**
     * @covers ::toString
     */
    public function testToString(): void
    {
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        $dt = '2020-09-08T09:51:12+05:00';

        $this->assertEquals('true', $this->sot->toString(true));
        $this->assertEquals('false', $this->sot->toString(false));
        $this->assertEquals('string', $this->sot->toString('string'));
        $this->assertEquals('123', $this->sot->toString(123));
        $this->assertEquals($dt, $this->sot->toString($date));

        $dt = 'Tuesday, 08-Sep-2020 09:51:12 GMT+0500';
        $this->sot->setDateTimeFormat(\DateTimeInterface::COOKIE);
        $this->assertEquals($dt, $this->sot->toString($date));
    }

    /**
     * @return array<string, array{string, mixed}>
     */
    public static function providePathValues(): array
    {
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        return [
            'encode things' => ['foo%20%40%2B%25%2F', 'foo @+%/'],
            'encode arrays' => ['test%2Cfoo%20%40%2B%25%2F', ['test', 'foo @+%/']],
            'encode date' => ['2020-09-08T09%3A51%3A12%2B05%3A00', $date],
            // Seems like this exposes the path serialization is vastly over simplified.
            'encode date array' => ['2020-09-08T09%3A51%3A12%2B05%3A00', [$date]],
            // 'encode models' => ['', [new MockModel(['test_int' => 123])]],
        ];
    }

    /**
     * @covers ::toPathValue
     * @dataProvider providePathValues
     */
    public function testToPathValue(string $expected, mixed $input): void
    {
        $this->assertEquals($expected, $this->sot->toPathValue($input));
    }

    /**
     * @covers ::processQuery
     */
    public function testProcessQuery(): void
    {
        $this->markTestIncomplete('still needed?');
    }

    /**
     * @covers ::sanitizeForSerialization
     */
    public function testSanitizeForSerialization(): void
    {
        $this->assertEquals(true, $this->sot->sanitizeForSerialization(true));
        $this->assertEquals('true', $this->sot->sanitizeForSerialization('true'));
        $this->assertEquals(123, $this->sot->sanitizeForSerialization(123));
        $this->assertEquals(123.23, $this->sot->sanitizeForSerialization(123.23));
        $scalar_array = [
            true,
            false,
            null,
            'true',
            'this is a string',
            123,
            123.23,
        ];
        $this->assertEquals($scalar_array, $this->sot->sanitizeForSerialization($scalar_array));
        $this->assertEquals((object)$scalar_array, $this->sot->sanitizeForSerialization((object)$scalar_array));

        // Dates.
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        $dt = '2020-09-08T09:51:12+05:00';
        $this->assertEquals('2020-09-08', $this->sot->sanitizeForSerialization($date, null, 'date'));
        $this->assertEquals($dt, $this->sot->sanitizeForSerialization($date));
        $this->assertEquals(['date' => $dt], $this->sot->sanitizeForSerialization(['date' => $date]));
        $this->assertEquals((object)['date' => $dt], $this->sot->sanitizeForSerialization((object)['date' => $date]));

        $model = new MockModel(['test_string' => 'test']);
        $this->assertEquals((object)['testString' => 'test'], $this->sot->sanitizeForSerialization($model));
        $this->assertEquals(
            [(object)['testString' => 'test'], (object)['testString' => 'test']],
            $this->sot->sanitizeForSerialization([$model, $model]),
        );
    }

    /**
     * @return array<array-key, array{string, string}>
     */
    public static function provideFileNames(): array
    {
        return [
            ['boot.ini', '....//....//boot.ini'],
            ['boot.ini', '....\\....\\boot.ini'],
            ['boot.ini', '..\..\boot.ini'],
            ['sun.gif', '../../sun.gif'],
            ['test.txt', 'test.txt'],
        ];
    }

    /**
     * @covers ::sanitizeFilename
     * @dataProvider provideFileNames
     */
    public function testSanitizeFilename(string $expected, string $input): void
    {
        $this->assertEquals($expected, $this->sot->sanitizeFilename($input));
    }

    /**
     * @covers ::toQueryValue
     */
    public function testToQueryValue(): void
    {
        $this->markTestIncomplete('TODO');
    }

    /**
     * @covers ::toHeaderValue
     */
    public function testToHeaderValue(): void
    {
        $this->markTestIncomplete('TODO');
    }

    /**
     * @covers ::setDateTimeFormat
     */
    public function testSetDateTimeFormat(): void
    {
        $date = new \DateTime('2020-09-08T09:51:12+05:00');
        $dt = 'Tuesday, 08-Sep-2020 09:51:12 GMT+0500';
        $this->sot->setDateTimeFormat(\DateTimeInterface::COOKIE);
        $this->assertEquals('2020-09-08', $this->sot->sanitizeForSerialization($date, null, 'date'));
        $this->assertEquals($dt, $this->sot->sanitizeForSerialization($date));
        $this->assertEquals([$dt], $this->sot->sanitizeForSerialization([$date]));
        $this->assertEquals((object)['date' => $dt], $this->sot->sanitizeForSerialization((object)['date' => $date]));
    }

    /**
     * @return array<array-key, array{class-string, array<mixed>, object}>
     */
    public static function provideBodyValues(): array
    {
        $data = [
            'test_int' => 123,
            'test_string' => 'my string',
            'test_bool' => true,
            'test_model' => new MockModel(['test_int' => 234]),
            'test_models' => [ new MockModel(['test_int' => 345])],
        ];
        $expected = [
            'testInt' => 123,
            'testString' => 'my string',
            'testBool' => true,
            'testModel' => (object)[
                'testInt' => 234,
            ],
            'testModels' => [
                (object)[
                    'testInt' => 345,
                ],
            ],
        ];
        return [
            'simple model' => [
                MockModel::class,
                $data,
                (object)$expected,
            ],
            'simple model ignores additional' => [
                MockModel::class,
                $data + ['a1' => 'b1'],
                (object)$expected,
            ],
            'simple additional model' => [
                MockModelAdditional::class,
                $data,
                (object)$expected,
            ],
            'additional model' => [
                MockModelAdditional::class,
                $data + ['a1' => 'b1'],
                (object)($expected + ['a1' => 'b1']),
            ],
        ];
    }

    /**
     * @covers ::toBodyValue
     * @dataProvider provideBodyValues
     * @param class-string $model_class
     * @param array<array-key, array{class-string, array<mixed>, object}> $data
     */
    public function testToBodyValue(string $model_class, array $data, object $expected): void
    {
        $this->assertEquals(
            json_encode($expected),
            (string) $this->sot->toBodyValue(new $model_class($data), 'application/json')
        );
    }
}
