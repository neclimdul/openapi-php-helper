<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests\Serialization;

use GuzzleHttp\Psr7\Utils;
use Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface as LegacyApiExceptionInterface;
use Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException;
use Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\ApiException;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer
 */
class DeserializerTest extends TestCase
{
    private Deserializer $sot;

    public function setUp(): void
    {
        parent::setUp();
        $this->sot = new Deserializer();
    }

    /**
     * @return array<string, array{mixed, string|null, string}>
     */
    public static function provideDeserialize(): array
    {
        $dt = '2020-09-08T09:51:12+05:00';
        $model_data = [
            'test_int' => 123,
            'test_string' => 'my string',
        ];
        $model_json = [
            'testInt' => 123,
            'testString' => 'my string',
        ];
        $model_data_additional = $model_data + ['additional_1' => 'foo'];
        $model_json_additional = $model_json + ['additional_1' => 'foo'];
        return [
            'null data' => [null, null, 'null'],
            'empty scalar array' => [[], '[]', 'string[]'],
            'scalar string' => ['a string', '"a string"', 'string'],
            'scalar array' => [['string1', 'string2'], '["string1","string2"]', 'string[]'],
            'scalar fixes questionable values' => [[''], '[null]', 'string[]'],
            // Exception...
            // [['string1', 'string2'], '{"k1":"string1","k2":"string2"}', 'string[]'],
            'freeform object/keyed array' => [
                ['k1' => 'string1', 'k2' => 'string2'],
                '{"k1":"string1","k2":"string2"}',
                'array<string,string>',
            ],
            'freeform object/keyed array w/ sub array' => [
                ['k1' => ['string1'], 'k2' => ['string2']],
                '{"k1":["string1"],"k2":["string2"]}',
                'array<string,string[]>',
            ],
            'mixed array' => [['test' => 'test'], '{"test": "test"}', 'array<string,mixed>'],
            'Datetime' => [new \DateTime($dt), '"' . $dt . '"', '\DateTime'],
            // This varies based on php version...
            // [new \DateTime($dt), '"2020-09-08T09:51:12.123456+05:00"', '\DateTime'],
            'DateTime array' => [[new \DateTime($dt)], '["' . $dt . '"]', '\DateTime[]'],
            'Bad date handling' => [[null], '[""]', '\DateTime[]'],
            // ---
            'Model' => [new MockModel($model_data), (string) json_encode($model_json), MockModel::class],
            'Model arrays' => [
                [new MockModel($model_data)],
                (string) json_encode([$model_json]),
                MockModel::class . '[]'
            ],
            'Model exclude extra' => [
                [new MockModel($model_data)],
                (string) json_encode([$model_json_additional]),
                MockModel::class . '[]',
            ],
            'Model Additional' => [
                [new MockModelAdditional($model_data_additional)],
                (string) json_encode([$model_json_additional]),
                MockModelAdditional::class . '[]',
            ],
            'Decode XML string' => ['a string', '<root>a string</root>', 'string', 'application/xml'],
            // XML probably needs more structural data but... maybe this works?
            'Decode XML string 2' => [
                ['k' => ['a string 1', 'a string 2']],
                '<root><k>a string 1</k><k>a string 2</k></root>',
                'array<string,string[]>',
                'application/xml',
            ],
            'Decode XML Model' => [
                new MockModelAdditional($model_data_additional),
                '<MockModelAdditional>
                    <testInt>123</testInt>
                    <testString>my string</testString>
                    <additional_1>foo</additional_1>
                </MockModelAdditional>',
                MockModelAdditional::class,
                'application/xml',
            ],
        ];
    }

    /**
     * @param mixed $expected
     * @param class-string $type
     * @covers ::deserialize
     * @covers ::doDeserialize
     * @dataProvider provideDeserialize
     */
    public function testDeserialize(
        mixed $expected,
        ?string $value,
        string $type,
        string $contentType = 'application/json'
    ): void {
        $this->assertEquals($expected, $this->sot->deserialize($value, $type, ['Content-Type' => [$contentType]]));
    }

    /**
     * @return array<string, array{class-string, string, string}>
     */
    public static function provideDeserializeErrors(): array
    {
        return [
            'bad object data' => [\InvalidArgumentException::class, '', MockModel::class],
            'bad array data' => [\InvalidArgumentException::class, '', MockModel::class . '[]'],
            'bad json' => [ApiSerializationException::class, 'bad{}json', MockModel::class . '[]'],
            'bad xml' => [ApiSerializationException::class, 'bad{}xml', MockModel::class . '[]', 'application/xml'],
        ];
    }

    /**
     * @param class-string<\Throwable> $expected
     * @param class-string $type
     * @covers ::deserialize
     * @dataProvider provideDeserializeErrors
     */
    public function testDeserializeErrors(
        $expected,
        string $value,
        $type,
        string $contentType = 'application/json'
    ): void {
        $this->expectException($expected);
        try {
            $this->sot->deserialize($value, $type, ['Content-Type' => [$contentType]]);
        } catch (\Throwable $exception) {
            if ($exception instanceof ApiSerializationException) {
                $this->assertEquals("Error decoding json: $value", $exception->getMessage());
            }
            throw $exception;
        }
    }


    /**
     * @covers ::deserialize
     */
    public function testDeserializeFile(): void
    {
        $filename = __DIR__ . '/../Fixtures/MockModel.php';
        $f = fopen($filename, 'r');
        $stream = Utils::streamFor($f);
        $t = $this->sot->deserialize($stream, \SplFileObject::class);
        $this->assertInstanceOf(\SplFileObject::class, $t);
        $t->rewind();
        $this->assertEquals((string)$stream, $t->fread((int) filesize($filename)));

        $t = $this->sot->deserialize($stream, '\SplFileObject');
        $this->assertInstanceOf(\SplFileObject::class, $t);
        $t->rewind();
        $this->assertEquals((string)$stream, $t->fread((int) filesize($filename)));

        $root = vfsStream::setup('test');
        $filename = $root->url() . '/foo.txt';
        file_put_contents($filename, 'test data');
        $f = fopen($filename, 'r');
        $stream = Utils::streamFor($f);
        $t = $this->sot->deserialize($stream, \SplFileObject::class);
        $this->assertInstanceOf(\SplFileObject::class, $t);
        $t->rewind();
        $this->assertEquals((string)$stream, $t->fread((int) filesize($filename)));
    }
}
