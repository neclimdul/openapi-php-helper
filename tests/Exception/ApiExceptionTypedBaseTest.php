<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Exception;

use Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\ApiExceptionTyped;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\ApiExceptionTypedBase
 */
class ApiExceptionTypedBaseTest extends TestCase
{
    private ApiExceptionInterface $sot;

    protected function setUp(): void
    {
        parent::setUp();
        $this->sot = new ApiExceptionTyped(
            'test message',
            123,
            ['foo' => ['bar']],
            'my body',
        );
    }

    /**
     * @covers ::__construct
     * @covers ::getResponseHeaders
     * @covers ::getResponseBody
     */
    public function testGeneral(): void
    {
        $this->assertInstanceOf(\Throwable::class, $this->sot);
        $this->assertEquals('test message', $this->sot->getMessage());
        $this->assertEquals(123, $this->sot->getCode());
        $this->assertEquals(['foo' => ['bar']], $this->sot->getResponseHeaders());
        $this->assertEquals('my body', $this->sot->getResponseBody());
    }

    /**
     * @covers ::setResponseObject
     * @covers ::getResponseObject
     */
    public function testGetResponseObject(): void
    {
        $this->assertNull($this->sot->getResponseObject());
        $obj = 'something something something';
        $this->sot->setResponseObject($obj);
        $this->assertSame($obj, $this->sot->getResponseObject());
        $this->sot->setResponseObject($this->sot);
        $this->assertSame($this->sot, $this->sot->getResponseObject());
    }
}
