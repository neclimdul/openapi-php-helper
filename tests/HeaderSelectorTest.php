<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests;

use Neclimdul\OpenapiPhp\Helper\HeaderSelector;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\HeaderSelector
 */
class HeaderSelectorTest extends TestCase
{
    /**
     * @var \Neclimdul\OpenapiPhp\Helper\HeaderSelector
     */
    private HeaderSelector $sot;

    public function setUp(): void
    {
        parent::setUp();
        $this->sot = new HeaderSelector();
    }

    /**
     * @return array{
     *   array<string, string>,
     *   array<string>,
     *   array<string>
     * }[]
     */
    public static function provideSelectHeaders(): array
    {
        return [
            [
                [
                    'Content-Type' => 'application/json',
                ],
                [],
                [],
            ],
            [
                [
                    'Content-Type' => 'application/json',
                ],
                [''],
                [],
            ],
            [
                [
                    'Accept' => 'application/hal+json',
                    'Content-Type' => 'application/json',
                ],
                ['', 'application/hal+json'],
                [],
            ],
            [
                [
                    'Accept' => 'application/hal+json',
                    'Content-Type' => 'application/json',
                ],
                ['application/hal+json'],
                [],
            ],
            [
                [
                    'Accept' => 'application/json,application/hal+json',
                    'Content-Type' => 'application/json',
                ],
                ['application/json', 'application/hal+json'],
                [],
            ],
            [
                [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                ['application/json', 'application/xml'],
                [],
            ],
            [
                [
                    'Accept' => 'application/xml,text/html,application/xhtml+xml',
                    'Content-Type' => 'application/json',
                ],
                ['application/xml', 'text/html', 'application/xhtml+xml'],
                [],
            ],
            [
                [
                    'Content-Type' => 'application/json',
                ],
                [''],
                [''],
            ],
            [
                [
                    'Content-Type' => 'application/json',
                ],
                [''],
                [''],
            ],
            [
                [
                    'Content-Type' => 'application/json',
                ],
                [''],
                ['application/json'],
            ],
            [
                [
                    'Content-Type' => 'application/xml,text/html,application/xhtml+xml',
                ],
                [''],
                ['application/xml', 'text/html', 'application/xhtml+xml'],
            ],
        ];
    }

    /**
     * @param array<string, string> $expect
     *   Expected header list.
     * @param array<string> $accept
     *   Test accept headers.
     * @param array<string> $content
     *   Test content headers.
     *
     * @covers ::selectHeaders
     * @covers ::doSelect
     * @dataProvider provideSelectHeaders
     */
    public function testSelectHeaders(array $expect, array $accept, array $content): void
    {
        $this->assertEquals($expect, $this->sot->selectHeaders($accept, $content));
    }

    /**
     * @param array<string, string> $expect
     *   Expected header list.
     * @param array<string> $accept
     *   Test accept headers.
     *
     * @covers ::selectAcceptHeader
     * @dataProvider provideSelectHeaders
     */
    public function testSelectAcceptHeader(array $expect, array $accept): void
    {
        $this->assertEquals($expect['Accept'] ?? null, $this->sot->selectAcceptHeader($accept));
    }

    /**
     * @param array<string, string> $expect
     *   Expected header list.
     * @param array<string> $accept
     *   Test accept headers.
     * @param array<string> $content
     *   Test content headers.
     *
     * @covers ::selectContentHeader
     * @dataProvider provideSelectHeaders
     */
    public function testSelectContentHeader(array $expect, array $accept, array $content): void
    {
        $this->assertEquals($expect['Content-Type'] ?? null, $this->sot->selectContentHeader($content));
    }

    /**
     * @param array<string, string> $expect
     *   Expected header list.
     * @param array<string> $accept
     *   Test accept headers.
     * @param array<string> $content
     *   Test content headers.
     *
     * @covers ::selectHeadersForMultipart
     * @covers ::doSelect
     * @dataProvider provideSelectHeaders
     */
    public function testSelectHeadersForMultipart(array $expect, array $accept, array $content): void
    {
        if (empty($content) || empty($content[0])) {
            // Without content type, default to form-data.
            $expect['Content-Type'] = 'multipart/form-data';
            $this->assertEquals($expect, $this->sot->selectHeadersForMultipart($accept, $content));
        } else {
            $this->assertEquals($expect, $this->sot->selectHeadersForMultipart($accept, $content));
        }
    }
}
