<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\Configuration;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\RequestHelperMock;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\RequestHelperTrait
 */
class RequestHelperTraitTest extends TestCase
{
    private MockHandler $mockHandler;

    /**
     * @var \GuzzleHttp\Client
     */
    private GuzzleClient $client;
    private RequestHelperMock $sot;
    private Configuration $config;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockHandler = new MockHandler();
        $this->client = new GuzzleClient(['handler' => HandlerStack::create($this->mockHandler)]);
        $this->config = new Configuration('example.com', '');
        $this->sot = new RequestHelperMock($this->client, $this->config);
    }

    /**
     * @return array<array<int>>
     */
    public static function provideSuccessErrorRequests(): array
    {
        return [
            '100 ' => [100],
            '120 ' => [120],
            '300 ' => [300],
            '350 ' => [350],
        ];
    }

    /**
     * @return array<array<int>>
     */
    public static function provideErrorRequests(): array
    {
        return [
            [400],
            [425],
            [500],
            [501],
        ];
    }

    /**
     * @covers ::setConfigForRequest
     * @covers ::createHttpClientOption
     */
    public function testCreateHttpClientOptions(): void
    {
        // Request debugging is broken by PSR7 support. I think that is ok?
        $this->markTestIncomplete('Broken by PSR support');
    }

    /**
     * @covers ::makeRequest
     * @covers ::getOpenApiClient
     * @covers ::setClientForRequest
     */
    public function testMakeRequest(): void
    {
        $request = new Request('GET', '/');
        $response = new Response();
        $this->mockHandler->append($response);
        $this->assertSame(
            $response,
            $this->sot->makeRequest($request)
        );
    }

    /**
     * @covers ::makeRequest
     * @covers ::setClientForRequest
     * @covers ::setExceptionForRequest
     * @covers ::generateExceptionFromException
     * @dataProvider provideErrorRequests
     */
    public function testMakeRequestException(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $request_e = new RequestException('Failure test', $request, $response);
        $this->mockHandler->append($request_e);
        try {
            $this->sot->makeRequest($request);
            $this->fail('Exception not thrown');
        } catch (ApiExceptionInterface $e) {
            $this->assertSame($request_e, $e->getPrevious());
            $this->assertEquals($code, $e->getCode());
            $this->assertEquals("[$code] Error connecting to the API (/)", $e->getMessage());
            $this->assertEquals("Failure test", $e->getPrevious()->getMessage());
            $this->assertNull($e->getResponseObject());
            $this->assertEquals('Failure body', $e->getResponseBody());
            $this->assertEquals(['failure' => ['failure header']], $e->getResponseHeaders());
        }
    }

    /**
     * @covers ::makeRequest
     * @covers ::setClientForRequest
     * @covers ::setExceptionForRequest
     * @dataProvider provideSuccessErrorRequests
     */
    public function testMakeRequestExceptionMore(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $this->mockHandler->append($response);
        try {
            $this->sot->makeRequest($request);
            $this->fail('Exception not thrown');
        } catch (ApiExceptionInterface $e) {
            $this->assertNull($e->getPrevious());
            $this->assertEquals($code, $e->getCode());
            $this->assertEquals("[$code] Error connecting to the API (/)", $e->getMessage());
            $this->assertNull($e->getResponseObject());
            $this->assertEquals('Failure body', $e->getResponseBody());
            $this->assertEquals(['failure' => ['failure header']], $e->getResponseHeaders());
        }
    }

    /**
     * @covers ::makeAsyncRequest
     * @covers ::getOpenApiClient
     * @covers ::setClientForRequest
     */
    public function testMakeAsyncRequest(): void
    {
        $request = new Request('GET', '/');
        $response = new Response();
        $this->mockHandler->append($response);
        $p2 = $this->sot->makeAsyncRequest($request, fn($e) => $e);
        // Response because caller is responsible for handling responses through
        // promise API.
        $this->assertEquals($response, $p2->wait());
    }

    /**
     * @covers ::makeAsyncRequest
     * @covers ::setClientForRequest
     * @covers ::setExceptionForRequest
     * @covers ::generateExceptionFromException
     * @dataProvider provideErrorRequests
     */
    public function testMakeAsyncRequestException(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $request_e = new RequestException('Failure test', $request, $response);
        $this->mockHandler->append($request_e);
        $obj = new MockModel();
        $handler = function (ApiExceptionInterface $e) use ($obj) {
            $e->setResponseObject($obj);
            return $e;
        };
        $p = $this->sot->makeAsyncRequest($request, $handler);
        try {
            $p->wait();
            $this->fail('Exception not thrown');
        } catch (ApiExceptionInterface $e) {
            $this->assertEquals($code, $e->getCode());
            $this->assertEquals("[$code] Error connecting to the API (/)", $e->getMessage());
            $this->assertSame($obj, $e->getResponseObject());
            $this->assertEquals('Failure body', $e->getResponseBody());
            $this->assertEquals(['failure' => ['failure header']], $e->getResponseHeaders());
        }
    }

    /**
     * @covers ::makeRequest
     * @covers ::setClientForRequest
     * @covers ::setClientForRequest
     * @dataProvider provideSuccessErrorRequests
     */
    public function testMakeAsyncRequestExceptionMore(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        //$request_e = new RequestException('Failure test', $request, $response);
        $this->mockHandler->append($response);
        $self = $this;
        $handler = function (ApiExceptionInterface $e) use ($self) {
            $self->fail('Exception should not happen.');
        };
        $p = $this->sot->makeAsyncRequest($request, $handler);
        try {
            $p
                ->then(function (Response $response) use ($self, $code) {
                    $self->assertEquals($code, $response->getStatusCode());
                    $self->assertEquals('Failure body', $response->getBody());
                })
                ->wait();
        } catch (ApiExceptionInterface) {
            $this->fail('Exception should not happen.');
        }
    }

    /**
     * @covers ::setSerializerForRequest
     * @covers ::responseToReturn
     */
    public function testResponseToReturn(): void
    {
        $response = new Response(
            200,
            ['Content-Type' => ['application-json']],
            ''
        );
        $this->assertEquals(
            [
                null,
                200,
                $response->getHeaders(),
            ],
            $this->sot->responseToReturn($response, null)
        );

        $response = new Response(
            200,
            ['Content-Type' => ['application-json']],
            '"this is a string"',
        );
        $this->assertEquals(
            [
                'this is a string',
                200,
                $response->getHeaders(),
            ],
            $this->sot->responseToReturn($response, 'string')
        );
    }
}
