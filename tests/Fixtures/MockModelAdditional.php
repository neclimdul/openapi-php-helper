<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Fixtures;

use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesInterface;
use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesTrait;

class MockModelAdditional extends MockModel implements AdditionalPropertiesInterface
{
    use AdditionalPropertiesTrait;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->additional = array_diff_key(
            $data,
            static::$attributeMap
        );
    }
}
