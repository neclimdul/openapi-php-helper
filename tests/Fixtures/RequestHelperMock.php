<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Fixtures;

use GuzzleHttp\ClientInterface;
use Neclimdul\OpenapiPhp\Helper\Configuration;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Neclimdul\OpenapiPhp\Helper\ObjectSerializer;
use Neclimdul\OpenapiPhp\Helper\RequestHelperTrait;

/**
 * Stub to allow access to RequestHelperTrait methods for testing.
 */
class RequestHelperMock
{
    /**
     * @use \Neclimdul\OpenapiPhp\Helper\RequestHelperTrait<\Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface,\Neclimdul\OpenapiPhp\Helper\Model\ModelInterface>
     */
    use RequestHelperTrait {
        makeRequest as public;
        makeAsyncRequest as public;
        responseToReturn as public;
    }

    /**
     * @param \GuzzleHttp\ClientInterface&\Psr\Http\Client\ClientInterface $client
     * @param \Neclimdul\OpenapiPhp\Helper\Configuration $configuration
     */
    public function __construct(
        ClientInterface $client,
        Configuration $configuration
    ) {
        $this->setClientForRequest($client);
        $this->setConfigForRequest($configuration);
        $objectSerializer = new ObjectSerializer(
            ModelInterface::class,
            ApiException::class
        );
        $this->setSerializerForRequest($objectSerializer);
        $this->setExceptionForRequest(ApiException::class);
    }
}
