<?php

/**
 * UpdatePetWithFormRequest class file.
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenApi\PetV2\Model;

use Neclimdul\OpenapiPhp\Helper\Model\ModelBase;
use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesTrait;
use Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait;
use OpenApi\PetV2\ObjectSerializer;

/**
 * UpdatePetWithFormRequest
 *
  * The version of the OpenAPI document: 1.0.7
 * Contact: apiteam@swagger.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.9.0
 *
 * @category Class
 * @package  OpenApi\PetV2
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<?string, ?mixed>
 * @psalm-suppress MixedReturnStatement
 * @psalm-suppress MixedInferredReturnType
 */
class UpdatePetWithFormRequest extends ModelBase implements ModelInterface, \ArrayAccess, \JsonSerializable
{
    use AdditionalPropertiesTrait;

    /**
     * @use \Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait<string, ?mixed>
     */
    use ArrayAccessTrait;

    public const DISCRIMINATOR = null;

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $openAPIModelName = 'updatePetWithForm_request';

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|class-string>
     * @psalm-var array<string, string|class-string>
     */
    protected static $openAPITypes = [
        'name' => 'string',
        'status' => 'string',
    ];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|null>
     * @psalm-var array<string, string|null>
     */
    protected static $openAPIFormats = [
        'name' => null,
        'status' => null,
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'name' => 'name',
        'status' => 'status',
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'name' => 'setName',
        'status' => 'setStatus',
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'name' => 'getName',
        'status' => 'getStatus',
    ];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = [])
    {
        $this->container = $this->collectAttributeValues($data);
    }

    /**
     * {@inheritdoc}
     */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * {@inheritdoc}
     */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
     * Gets name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string|null $name Updated name of the pet
     *
     * @return self
     */
    public function setName(?string $name): UpdatePetWithFormRequest
    {
        $this->container['name'] = $name;

        return $this;
    }
    /**
     * Gets status
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string|null $status Updated status of the pet
     *
     * @return self
     */
    public function setStatus(?string $status): UpdatePetWithFormRequest
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR
        );
    }
}
