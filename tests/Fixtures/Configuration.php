<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Fixtures;

use Neclimdul\OpenapiPhp\Helper\Configuration as ConfigurationBase;

class Configuration extends ConfigurationBase
{
    /**
     * {@inheritDoc}
     */
    public function getHostSettings(): array
    {
        return [
            [
                'url' => 'https://api.example.com',
                'description' => 'No description provided',
            ],
            [
                'url' => 'https://{id}.api.example.com/{version}/',
                'description' => 'No description provided',
                'variables' => [
                    'id' => [
                        'default_value' => 123,
                    ],
                    'version' => [
                        'enum_values' => [1, 2],
                        'default_value' => 1
                    ]
                ]
            ],
        ];
    }
}
