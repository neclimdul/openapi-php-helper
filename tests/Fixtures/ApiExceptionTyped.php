<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Fixtures;

use Neclimdul\OpenapiPhp\Helper\ApiExceptionTypedBase;

class ApiExceptionTyped extends ApiExceptionTypedBase
{
}
