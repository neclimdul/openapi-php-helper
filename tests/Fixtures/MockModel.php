<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Fixtures;

use Neclimdul\OpenapiPhp\Helper\Model\ModelBase;

class MockModel extends ModelBase
{
    /**
     * @var string|null
     */
    public const DISCRIMINATOR = null;

    /**
     * {@inheritDoc}
     */
    protected static $openAPIModelName = 'MockModel';

    /**
     * {@inheritDoc}
     */
    protected static $openAPITypes = [
        'test_int' => 'int',
        'test_string' => 'string',
        'test_bool' => 'bool',
        'test_model' => \Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel::class,
        'test_models' => '\Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel[]',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $openAPIFormats = [
        'test_int' => null,
        'test_string' => null,
        'test_bool' => null,
        'test_model' => null,
        'test_models' => null,
    ];

    /**
     * {@inheritDoc}
     */
    protected static $attributeMap = [
        'test_int' => 'testInt',
        'test_string' => 'testString',
        'test_bool' => 'testBool',
        'test_model' => 'testModel',
        'test_models' => 'testModels',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $setters = [
        'test_int' => 'setTestInt',
        'test_string' => 'setTestString',
        'test_bool' => 'setTestBool',
        'test_model' => 'setTestModel',
        'test_models' => 'setTestModels',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $getters = [
        'test_int' => 'getTestInt',
        'test_string' => 'getTestString',
        'test_bool' => 'getTestBool',
        'test_model' => 'getTestModel',
        'test_models' => 'getTestModels',
    ];

    /**
     * Construct a mock model.
     *
     * @param array<string, mixed> $data
     *   Raw model data.
     */
    public function __construct(array $data = [])
    {
        $this->container = $this->collectAttributeValues($data);
    }

    public function setTestInt(int $value): self
    {
        $this->container['test_int'] = $value;
        return $this;
    }

    public function getTestInt(): ?int
    {
        return $this->container['test_int'];
    }

    public function setTestString(string $value): self
    {
        $this->container['test_string'] = $value;
        return $this;
    }

    public function getTestString(): ?string
    {
        return $this->container['test_string'];
    }

    public function setTestBool(bool $value): self
    {
        $this->container['test_bool'] = $value;
        return $this;
    }

    public function getTestBool(): ?bool
    {
        return $this->container['test_bool'];
    }

    public function setTestModel(self $value): self
    {
        $this->container['test_model'] = $value;
        return $this;
    }
    public function getTestModel(): ?self
    {
        return $this->container['test_model'];
    }

    /**
     * @param self[] $value
     * @return $this
     */
    public function setTestModels(array $value): self
    {
        $this->container['test_models'] = $value;
        return $this;
    }

    /**
     * @return self[]|null
     */
    public function getTestModels(): ?array
    {
        return $this->container['test_models'];
    }
}
