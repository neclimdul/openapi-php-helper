<?php

/**
 * Pet class file.
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenApi\PetV3\Model;

use Neclimdul\OpenapiPhp\Helper\Model\ModelBase;
use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesTrait;
use Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait;
use OpenApi\PetV3\ObjectSerializer;

/**
 * Pet
 *
  * The version of the OpenAPI document: 1.0.19
 * Contact: apiteam@swagger.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.9.0
 *
 * @category Class
 * @package  OpenApi\PetV3
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<?string, ?mixed>
 * @psalm-suppress MixedReturnStatement
 * @psalm-suppress MixedInferredReturnType
 */
class Pet extends ModelBase implements ModelInterface, \ArrayAccess, \JsonSerializable
{
    use AdditionalPropertiesTrait;

    /**
     * @use \Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait<string, ?mixed>
     */
    use ArrayAccessTrait;

    public const DISCRIMINATOR = null;
    public const STATUS_AVAILABLE = 'available';
    public const STATUS_PENDING = 'pending';
    public const STATUS_SOLD = 'sold';

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $openAPIModelName = 'Pet';

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|class-string>
     * @psalm-var array<string, string|class-string>
     */
    protected static $openAPITypes = [
        'id' => 'int',
        'name' => 'string',
        'category' => '\OpenApi\PetV3\Model\Category',
        'photo_urls' => 'string[]',
        'tags' => '\OpenApi\PetV3\Model\Tag[]',
        'status' => 'string',
    ];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|null>
     * @psalm-var array<string, string|null>
     */
    protected static $openAPIFormats = [
        'id' => 'int64',
        'name' => null,
        'category' => null,
        'photo_urls' => null,
        'tags' => null,
        'status' => null,
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'name' => 'name',
        'category' => 'category',
        'photo_urls' => 'photoUrls',
        'tags' => 'tags',
        'status' => 'status',
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'name' => 'setName',
        'category' => 'setCategory',
        'photo_urls' => 'setPhotoUrls',
        'tags' => 'setTags',
        'status' => 'setStatus',
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'name' => 'getName',
        'category' => 'getCategory',
        'photo_urls' => 'getPhotoUrls',
        'tags' => 'getTags',
        'status' => 'getStatus',
    ];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = [])
    {
        $this->container = $this->collectAttributeValues($data);
    }

    /**
     * {@inheritdoc}
     */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties['name'] = "'name' can't be null";
        }
        if ($this->container['photo_urls'] === null) {
            $invalidProperties['photo_urls'] = "'photo_urls' can't be null";
        }
        $allowedValues = $this->getStatusAllowableValues();
        $value = $this->container['status'];
        if (!is_null($value) && !in_array($value, $allowedValues, true)) {
            $invalidProperties['status'] = sprintf(
                "invalid value '%s' for 'status', must be one of '%s'",
                $value,
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * {@inheritdoc}
     */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
     * Gets allowable values of the enum.
     *
     * @return scalar[]
     */
    public function getStatusAllowableValues(): array
    {
        return [
            self::STATUS_AVAILABLE,
            self::STATUS_PENDING,
            self::STATUS_SOLD,
        ];
    }

    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id
     *
     * @return self
     */
    public function setId(?int $id): Pet
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
     * Gets name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return self
     */
    public function setName(string $name): Pet
    {
        $this->container['name'] = $name;

        return $this;
    }
    /**
     * Gets category
     *
     * @return \OpenApi\PetV3\Model\Category|null
     */
    public function getCategory(): ?\OpenApi\PetV3\Model\Category
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     *
     * @param \OpenApi\PetV3\Model\Category|null $category category
     *
     * @return self
     */
    public function setCategory(?\OpenApi\PetV3\Model\Category $category): Pet
    {
        $this->container['category'] = $category;

        return $this;
    }
    /**
     * Gets photo_urls
     *
     * @return string[]
     */
    public function getPhotoUrls(): array
    {
        return $this->container['photo_urls'];
    }

    /**
     * Sets photo_urls
     *
     * @param string[] $photo_urls photo_urls
     *
     * @return self
     */
    public function setPhotoUrls(array $photo_urls): Pet
    {
        $this->container['photo_urls'] = $photo_urls;

        return $this;
    }
    /**
     * Gets tags
     *
     * @return \OpenApi\PetV3\Model\Tag[]|null
     */
    public function getTags(): ?array
    {
        return $this->container['tags'];
    }

    /**
     * Sets tags
     *
     * @param \OpenApi\PetV3\Model\Tag[]|null $tags tags
     *
     * @return self
     */
    public function setTags(?array $tags): Pet
    {
        $this->container['tags'] = $tags;

        return $this;
    }
    /**
     * Gets status
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string|null $status pet status in the store
     *
     * @return self
     */
    public function setStatus(?string $status): Pet
    {
        $allowedValues = $this->getStatusAllowableValues();
        if (!is_null($status) && !in_array($status, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'status', must be one of '%s'",
                    $status,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR
        );
    }
}
