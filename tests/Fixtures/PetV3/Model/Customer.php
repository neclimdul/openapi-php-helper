<?php

/**
 * Customer class file.
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenApi\PetV3\Model;

use Neclimdul\OpenapiPhp\Helper\Model\ModelBase;
use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesTrait;
use Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait;
use OpenApi\PetV3\ObjectSerializer;

/**
 * Customer
 *
  * The version of the OpenAPI document: 1.0.19
 * Contact: apiteam@swagger.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.9.0
 *
 * @category Class
 * @package  OpenApi\PetV3
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<?string, ?mixed>
 * @psalm-suppress MixedReturnStatement
 * @psalm-suppress MixedInferredReturnType
 */
class Customer extends ModelBase implements ModelInterface, \ArrayAccess, \JsonSerializable
{
    use AdditionalPropertiesTrait;

    /**
     * @use \Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait<string, ?mixed>
     */
    use ArrayAccessTrait;

    public const DISCRIMINATOR = null;

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $openAPIModelName = 'Customer';

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|class-string>
     * @psalm-var array<string, string|class-string>
     */
    protected static $openAPITypes = [
        'id' => 'int',
        'username' => 'string',
        'address' => '\OpenApi\PetV3\Model\Address[]',
    ];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|null>
     * @psalm-var array<string, string|null>
     */
    protected static $openAPIFormats = [
        'id' => 'int64',
        'username' => null,
        'address' => null,
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'username' => 'username',
        'address' => 'address',
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'username' => 'setUsername',
        'address' => 'setAddress',
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'username' => 'getUsername',
        'address' => 'getAddress',
    ];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = [])
    {
        $this->container = $this->collectAttributeValues($data);
    }

    /**
     * {@inheritdoc}
     */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * {@inheritdoc}
     */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id
     *
     * @return self
     */
    public function setId(?int $id): Customer
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
     * Gets username
     *
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->container['username'];
    }

    /**
     * Sets username
     *
     * @param string|null $username username
     *
     * @return self
     */
    public function setUsername(?string $username): Customer
    {
        $this->container['username'] = $username;

        return $this;
    }
    /**
     * Gets address
     *
     * @return \OpenApi\PetV3\Model\Address[]|null
     */
    public function getAddress(): ?array
    {
        return $this->container['address'];
    }

    /**
     * Sets address
     *
     * @param \OpenApi\PetV3\Model\Address[]|null $address address
     *
     * @return self
     */
    public function setAddress(?array $address): Customer
    {
        $this->container['address'] = $address;

        return $this;
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR
        );
    }
}
