<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Fixtures;

use Neclimdul\OpenapiPhp\Helper\ApiExceptionBase;

class ApiException extends ApiExceptionBase
{
}
