<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests\Logging;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Neclimdul\OpenapiPhp\Helper\Logging\Error;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseSync;
use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Logging\Error
 */
class ErrorTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @covers ::logError
     */
    public function testLogError(): void
    {
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::ERROR, 'Client error: "GET /test" resulted in a "404 Not Found" response')
            ->shouldBeCalledOnce();
        $response = new ApiResponseSync(
            new Request('get', '/test'),
            new Response(404),
            new Deserializer(),
            new ResponseTypeMap([]),
        );
        Error::logError(
            $logger->reveal(),
            $response,
        );
    }
}
