<?php

namespace Neclimdul\OpenapiPhp\Helper\Tests;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Neclimdul\OpenapiPhp\Helper\Client;
use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Http\Client\ClientExceptionInterface;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Client
 */
class ClientTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @var \Neclimdul\OpenapiPhp\Helper\Client
     */
    private Client $sot;

    private MockHandler $mockHandler;

    /**
     * @var \GuzzleHttp\Client
     */
    private GuzzleClient $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->mockHandler = new MockHandler();
        $this->client = new GuzzleClient(['handler' => HandlerStack::create($this->mockHandler)]);
        $this->sot = new Client(
            $this->client,
            new Deserializer(),
        );
    }

    /**
     * @return array<array<int>>
     */
    public static function provideSuccessErrorRequests(): array
    {
        return [
            '100 ' => [100],
            '120 ' => [120],
            '300 ' => [300],
            '350 ' => [350],
        ];
    }

    /**
     * @return array<array<int>>
     */
    public static function provideErrorRequests(): array
    {
        return [
            [400],
            [425],
            [500],
            [501],
        ];
    }

    /**
     * @covers ::__construct
     * @covers ::makeRequest
     */
    public function testMakeRequest(): void
    {
        $response = new Response(body: '{"testString": "foobar"}');
        $this->mockHandler->append($response);
        $apiResponse = $this->sot->makeRequest(
            new Request('GET', '/'),
            new ResponseTypeMap(['default' => ['*/*' => ['type' => MockModel::class]]]),
            fn($e) => $e,
        );
        $this->assertSame(
            $response,
            $apiResponse->getResponse(),
        );
        $this->assertEquals(
            'foobar',
            $apiResponse->getData()->getTestString(),
        );
    }

    /**
     * Note: These status codes aren't errors, but it provides a good test of exception handling.
     *
     * @covers ::makeRequest
     * @dataProvider provideErrorRequests
     */
    public function testMakeRequestException(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $request_e = new RequestException('Failure test', $request, $response);
        $this->mockHandler->append($request_e);

        try {
            $this->sot->makeRequest(
                $request,
                new ResponseTypeMap(['default' => ['*/*' => ['type' => 'string']]]),
                fn($e) => $e,
            );
            $this->fail('Exception not thrown');
        } catch (ClientExceptionInterface $e) {
            $this->assertSame($request_e, $e);
        }
    }

    /**
     * @covers ::makeRequest
     * @dataProvider provideErrorRequests
     */
    public function testMakeRequestErrors(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $this->mockHandler->append($response);

        $apiResponse = $this->sot->makeRequest(
            $request,
            new ResponseTypeMap(['default' => ['*/*' => ['type' => 'string']]]),
            fn($e) => $e,
        );
        $this->assertSame(
            $response,
            $apiResponse->getResponse(),
        );
    }

    /**
     * @covers ::makeRequest
     * @covers ::doAsyncRequest
     * @dataProvider provideSuccessErrorRequests
     */
    public function testMakeRequestExceptionMore(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $this->mockHandler->append($response);
        $this->assertSame(
            $response,
            $this->sot->makeRequest(
                $request,
                new ResponseTypeMap(['default' => ['*/*' => ['type' => 'string']]]),
                fn($e) => $e,
            )->getResponse(),
        );
    }

    /**
     * @covers ::makeRequest
     * @covers ::doAsyncRequest
     */
    public function testMakeAsyncRequest(): void
    {
        $request = new Request('GET', '/');
        $response = new Response();
        $this->mockHandler->append($response);
        $r = $this->sot->makeRequest(
            $request,
            new ResponseTypeMap(['default' => ['*/*' => ['type' => 'string']]]),
            async: true,
        );
        $p2 = $r->getPromise();
        // Response because caller is responsible for handling responses through
        // promise API.
        $this->assertEquals($response, $p2->wait());
    }

    /**
     * @covers ::makeRequest
     * @covers ::doAsyncRequest
     * @dataProvider provideErrorRequests
     */
    public function testMakeAsyncRequestException(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], '{}');
        $request_e = new RequestException('Failure test', $request, $response);
        $this->mockHandler->append($request_e);
        $obj = new MockModel();
        $handler = function (ClientExceptionInterface $e) {
            return $e;
        };
        $r = $this->sot->makeRequest(
            $request,
            new ResponseTypeMap(['default' => ['*/*' => ['type' => 'string']]]),
            $handler,
            true,
        );
        try {
            $p = $r->getPromise();
            $p->wait();
            $this->fail('Exception not thrown');
        } catch (ClientExceptionInterface $e) {
            $this->assertSame($request_e, $e);
        }
    }

    /**
     * @covers ::makeRequest
     * @dataProvider provideSuccessErrorRequests
     */
    public function testMakeAsyncRequestExceptionMore(int $code): void
    {
        $request = new Request('GET', '/');
        $response = new Response($code, ['failure' => ['failure header']], 'Failure body');
        $this->mockHandler->append($response);
        $self = $this;
        $handler = function (ClientExceptionInterface $e) use ($self) {
            $self->fail('Exception should not happen.');
        };
        $r = $this->sot->makeRequest(
            $request,
            new ResponseTypeMap(['default' => ['*/*' => ['type' => 'string']]]),
            $handler,
            true,
        );
        try {
            $p = $r->getPromise();
            $p
                ->then(function (Response $response) use ($self, $code) {
                    $self->assertEquals($code, $response->getStatusCode());
                    $self->assertEquals('Failure body', $response->getBody());
                })
                ->wait();
        } catch (ClientExceptionInterface) {
            $this->fail('Exception should not happen.');
        }
    }
}
