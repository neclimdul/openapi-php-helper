<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Model;

use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Model\ModelBase
 */
class ModelBaseTest extends TestCase
{
    private MockModel $sot;

    protected function setUp(): void
    {
        parent::setUp();
        $this->sot = new MockModel();
    }

    /**
     * @covers ::getModelName
     */
    public function testGetModelName(): void
    {
        $this->assertEquals('MockModel', $this->sot->getModelName());
    }

    /**
     * @covers ::setters
     */
    public function testSetters(): void
    {
        $this->assertEquals(
            [
                'test_int' => 'setTestInt',
                'test_string' => 'setTestString',
                'test_bool' => 'setTestBool',
                'test_model' => 'setTestModel',
                'test_models' => 'setTestModels',
            ],
            $this->sot->setters()
        );
    }

    /**
     * @covers ::getters
     */
    public function testGetters(): void
    {
        $this->assertEquals(
            [
                'test_int' => 'getTestInt',
                'test_string' => 'getTestString',
                'test_bool' => 'getTestBool',
                'test_model' => 'getTestModel',
                'test_models' => 'getTestModels',
            ],
            $this->sot->getters()
        );
    }

    /**
     * @covers ::attributeMap
     */
    public function testAttributeMap(): void
    {
        $this->assertEquals(
            [
                'test_int' => 'testInt',
                'test_string' => 'testString',
                'test_bool' => 'testBool',
                'test_model' => 'testModel',
                'test_models' => 'testModels',
            ],
            $this->sot->attributeMap()
        );
    }

    /**
     * @covers ::openAPITypes
     */
    public function testOpenAPITypes(): void
    {
        $this->assertEquals(
            [
                'test_int' => 'int',
                'test_string' => 'string',
                'test_bool' => 'bool',
                'test_model' => \Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel::class,
                'test_models' => '\Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel[]',
            ],
            $this->sot->openAPITypes()
        );
    }

    /**
     * @covers ::openAPIFormats
     */
    public function testOpenAPIFormats(): void
    {
        $this->assertEquals(
            [
                'test_int' => null,
                'test_string' => null,
                'test_bool' => null,
                'test_model' => null,
                'test_models' => null,
            ],
            $this->sot->openAPIFormats()
        );
    }

    /**
     * @covers ::collectAttributeValues
     */
    public function testCollectAttributeValues(): void
    {
        $m0 = new MockModel();
        $this->assertNull($m0->getTestInt());
        $this->assertNull($m0->getTestString());
        $this->assertNull($m0->getTestBool());
        $this->assertNull($m0->getTestModel());
        $this->assertNull($m0->getTestModels());
        $m1 = new MockModel(['test_int' => 234]);
        $m2 = new MockModel(['test_int' => 345]);
        $data = [
            'test_int' => 123,
            'test_string' => 'my string',
            'test_bool' => true,
            'test_model' => $m1,
            'test_models' => [$m2],
        ];
        $t = new MockModel($data);
        $this->assertEquals(
            123,
            $t->getTestInt()
        );
        $this->assertEquals(
            'my string',
            $t->getTestString()
        );
        $this->assertEquals(
            true,
            $t->getTestBool()
        );
        $this->assertEquals(
            $m1,
            $t->getTestModel()
        );
        $this->assertEquals(
            [$m2],
            $t->getTestModels()
        );
    }
}
