<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Model;

use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesTrait
 */
class AdditionalPropertiesTraitTest extends TestCase
{
    /**
     * @var \Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional
     */
    private MockModelAdditional $sot;

    protected function setUp(): void
    {
        parent::setUp();
        $this->sot = new MockModelAdditional();
    }

    /**
     * @covers ::setAdditionalProperty
     * @covers ::getAdditionalProperty
     * @covers ::hasAdditionalProperty
     */
    public function testSetAdditionalProperty(): void
    {
        $v = 'testvalue';
        $this->assertFalse($this->sot->hasAdditionalProperty('test_prop'));
        $this->sot->setAdditionalProperty('test_prop', $v);
        $this->assertTrue($this->sot->hasAdditionalProperty('test_prop'));
        $this->assertEquals(
            $v,
            $this->sot->getAdditionalProperty('test_prop')
        );
    }

    /**
     * @covers ::setAdditionalProperty
     */
    public function testSetAdditionalPropertyError(): void
    {
        $this->expectException(\AssertionError::class);
        $this->sot->setAdditionalProperty('test_int', 1);
    }

    /**
     * @covers ::setAdditionalProperties
     * @covers ::getAdditionalProperties
     */
    public function testSetAdditionalProperties(): void
    {
        $v = 'testvalue';
        $this->sot->setAdditionalProperty('test_prop', $v);
        $this->sot->setAdditionalProperties(['test_prop2' => $v]);
        $this->assertEquals(
            [
                'test_prop' => $v,
                'test_prop2' => $v,
            ],
            $this->sot->getAdditionalProperties()
        );
    }

    /**
     * @covers ::setAdditionalProperties
     */
    public function testSetAdditionalPropertiesError(): void
    {
        $this->expectException(\AssertionError::class);
        $this->sot->setAdditionalProperties(['test_int' => 1]);
    }
}
