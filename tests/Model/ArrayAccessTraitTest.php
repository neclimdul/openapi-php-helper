<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Tests\Model;

use Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait;
use Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTypedTrait;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModel;
use Neclimdul\OpenapiPhp\Helper\Tests\Fixtures\MockModelAdditional;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhp\Helper\Model\ArrayAccessTrait
 */
class ArrayAccessTraitTest extends TestCase
{
    public function testArrayAccess(): void
    {
        $sot = new class () extends MockModelAdditional implements \ArrayAccess {
            /** @use ArrayAccessTrait<string, mixed> */
            use ArrayAccessTrait;
        };
        // Normal property.
        $this->assertFalse(isset($sot['test_int']));
        $sot['test_int'] = 123;
        $this->assertEquals(123, $sot['test_int']);
        $this->assertEquals(123, $sot->getTestInt());
        unset($sot['test_int']);
        $this->assertFalse(isset($sot['test_int']));

        // Additional property.
        $this->assertFalse(isset($sot['test']));
        $sot['test'] = 'foobar';
        $this->assertEquals('foobar', $sot['test']);
        $this->assertEquals('foobar', $sot->getAdditionalProperty('test'));
        unset($sot['test']);
        $this->assertFalse(isset($sot['test']));
    }

    public function testArrayAccessDontAppend(): void
    {
        $sot = new class () extends MockModelAdditional implements \ArrayAccess {
            /** @use ArrayAccessTrait<string, mixed> */
            use ArrayAccessTrait;
        };
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(
            'Appending to a model does not make sense. Provide an explicit property instead.'
        );
        $sot[] = 'foobar';
    }

    public function testArrayAccessBadKey(): void
    {
        $sot = new class () extends MockModel implements \ArrayAccess {
            /** @use ArrayAccessTrait<string, mixed> */
            use ArrayAccessTrait;
        };
        $this->expectException(\InvalidArgumentException::class);
        $sot['test'] = 'foobar';
    }
}
