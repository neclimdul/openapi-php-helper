<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

/**
 * @deprecated
 * @psalm-suppress DeprecatedInterface
 */
abstract class ApiExceptionBase extends \Exception implements ApiExceptionInterface
{
    /**
     * The deserialized response object.
     *
     * This can be almost anything because it is what ever was defined by the
     * API or sometimes null if it wasn't parseable or there was no data. This
     * includes models, strings, collections(arrays structured or unstructured),
     * or even any scalar value.
     *
     * @var mixed
     */
    protected $responseObject = null;

    /**
     * Constructor
     *
     * @param string $message
     *   Error message.
     * @param int $code
     *   HTTP status code.
     * @param string[][] $responseHeaders
     *   HTTP response header.
     * @param string|null $responseBody
     *   HTTP decoded body of the server response as a string.
     * @param \Throwable|null $previous
     *   The previous throwable used for the exception chaining.
     */
    public function __construct(
        string $message = "",
        int $code = 0,
        protected array $responseHeaders = [],
        protected ?string $responseBody = null,
        ?\Throwable $previous = null
    ) {
        // TODO should this allow strings so it can accept weird MS codes like 404.1?
        parent::__construct($message, $code, $previous);
    }

    /**
     * {@inheritDoc}
     */
    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * {@inheritDoc}
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * {@inheritDoc}
     */
    public function setResponseObject(mixed $obj): void
    {
        $this->responseObject = $obj;
    }

    /**
     * {@inheritDoc}
     */
    public function getResponseObject()
    {
        return $this->responseObject;
    }
}
