<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

use Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Neclimdul\OpenapiPhp\Helper\Serialization\Deserializer;
use Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface;
use Neclimdul\OpenapiPhp\Helper\Serialization\Serializer;
use Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface;

/**
 * Base object serialization class.
 *
 * @deprecated Use individual Serializer and Deserializer classes.
 *
 * Contents of the file originally authored by the Swagger and OpenAPI codegen
 * teams.
 *
 * @template-covariant TException of \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
 * @template TModelInterface of \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
 * @implements \Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface<TModelInterface>
 */
class ObjectSerializer implements SerializerInterface, DeserializerInterface
{
    /**
     * @var \Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface<TModelInterface>
     */
    private SerializerInterface $serializer;

    private DeserializerInterface $deserializer;

    /**
     * Construct an ObjectSerializer.
     *
     * @param class-string<TModelInterface> $modelInterface
     *   The model interface used by the library for models.
     * @param class-string<TException> $exceptionClass
     *   The exception class to be thrown during failures.
     * @param string $dateTimeFormat
     *   The format to be used when formatting date time objects.
     */
    public function __construct(
        private readonly string $modelInterface,
        private readonly string $exceptionClass,
        string $dateTimeFormat = \DateTimeInterface::ATOM,
    ) {
        $this->serializer = new Serializer(
            $this->modelInterface,
            $dateTimeFormat,
        );
        $this->deserializer = new Deserializer();
    }

    /**
     * Get the related serializer.
     *
     * @return \Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface<TModelInterface>
     */
    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    /**
     * Get the related deserializer.
     *
     * @return \Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface
     */
    public function getDeserializer(): DeserializerInterface
    {
        return $this->deserializer;
    }

    /**
     * {@inheritDoc}
     */
    public function setDateTimeFormat(string $format): void
    {
        $this->serializer->setDateTimeFormat($format);
    }

    /**
     * {@inheritDoc}
     */
    public function sanitizeForSerialization(mixed $data, ?string $type = null, ?string $format = null)
    {
        return $this->serializer->sanitizeForSerialization($data, $type, $format);
    }

    /**
     * {@inheritDoc}
     */
    public function sanitizeFilename(string $filename): string
    {
        return $this->serializer->sanitizeFilename($filename);
    }

    /**
     * {@inheritDoc}
     */
    public function toPathValue($value): string
    {
        return $this->serializer->toPathValue($value);
    }

    /**
     * {@inheritDoc}
     */
    public function toQueryValue($value): string
    {
        return $this->serializer->toQueryValue($value);
    }

    /**
     * {@inheritDoc}
     */
    public function processQuery(
        string $name,
        mixed $value,
        string $style
    ): array {
        return $this->serializer->processQuery($name, $value, $style);
    }

    /**
     * {@inheritDoc}
     */
    public function toHeaderValue($value): string
    {
        return $this->serializer->toHeaderValue($value);
    }

    /**
     * {@inheritDoc}
     */
    public function fileToFormValue($file): array
    {
        return $this->serializer->fileToFormValue($file);
    }

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the http body (form parameter). If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     *
     * @param scalar|\SplFileObject|ModelInterface $value
     *   The value of the form parameter
     *
     * @return string
     *   The form value prepared as a string.
     * @throws TException
     *   Thrown if unable to convert into a string.
     */
    public function toFormValue($value): string
    {
        try {
            return $this->serializer->toFormValue($value);
        } catch (ApiSerializationException $e) {
            throw new $this->exceptionClass(
                $e->getMessage(),
                $e->getCode(),
                previous: $e,
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function toString($value): string
    {
        return $this->serializer->toString($value);
    }

    /**
     * {@inheritDoc}
     */
    public function toBodyValue(mixed $object, string $contentType)
    {
        return $this->serializer->toBodyValue($object, $contentType);
    }

    /**
     * Serialize an array to a string.
     *
     * @see https://swagger.io/specification/#style-examples
     *
     * @param string|scalar[] $collection
     *   Collection to serialize to a string.
     * @param string $style
     *   The format use for serialization (csv, ssv, tsv, pipes, multi).
     * @param bool $allowCollectionFormatMulti
     *   Allow collection format to be a multidimensional array.
     *
     * @return string
     * @throws TException
     *   Thrown if php is unable to convert the collection into a string.
     */
    public function serializeCollection(
        $collection,
        string $style,
        bool $allowCollectionFormatMulti = false
    ): string {
        try {
            return $this->serializer->serializeCollection($collection, $style, $allowCollectionFormatMulti);
        } catch (ApiSerializationException $e) {
            throw new $this->exceptionClass(
                $e->getMessage(),
                $e->getCode(),
                previous: $e,
            );
        }
    }

    /**
     * Deserialize a JSON string into an object.
     *
     * @template T
     * @param \Stringable|string|null $data
     *   Object or primitive to be deserialized.
     * @param string $type
     *   Some php type that be returned.
    * @param array<array-key, string[]> $httpHeaders
     *   A list of headers from the response.
     * @phpstan-param class-string<T>|string $type
     *
     * @return mixed
     *   A single or an array of $type instances.
     * @phpstan-return ($data is null ? null : ($type is class-string<T> ? T : scalar))
     * @throws TException
     */
    public function deserialize(
        mixed $data,
        string $type,
        array $httpHeaders = ['Content-Type' => ['application/json']]
    ): mixed {
        try {
            return $this->deserializer->deserialize($data, $type, $httpHeaders);
        } catch (ApiSerializationException $e) {
            throw new $this->exceptionClass(
                $e->getMessage(),
                $e->getCode(),
                [],
                $data,
                previous: $e,
            );
        }
    }
}
