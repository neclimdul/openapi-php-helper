<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Response;

use Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * A synchronous API response.
 */
class ApiResponseSync extends ApiResponse implements ApiResponseInterface
{
    /**
     * Create a synchronous API response.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     *   Related request.
     * @param \Psr\Http\Message\ResponseInterface $response
     *   Native API Response
     * @param \Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface $deserializer
     *   Deserialization service.
     * @param \Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap $returnTypes
     *   Return types.
     */
    public function __construct(
        RequestInterface $request,
        private readonly ResponseInterface $response,
        DeserializerInterface $deserializer,
        ResponseTypeMap $returnTypes,
    ) {
        parent::__construct($request, $deserializer, $returnTypes);
    }

    /**
     * {@inheritDoc}
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }
}
