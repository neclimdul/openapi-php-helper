<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Response;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * API response wrapper interface.
 *
 */
interface ApiResponseInterface
{
    /**
     * Get the request that triggered the response.
     *
     * @return \Psr\Http\Message\RequestInterface
     */
    public function getRequest(): RequestInterface;

    /**
     * Get native response from the request.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getResponse(): ResponseInterface;

    /**
     * Is the response a success.
     *
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * Get the deserialized result of the request.
     *
     * @return mixed
     *   The deserialized version of the response.
     * @throws \Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException
     *   Thrown if there is a problem decoding or deserializing the response.
     */
    public function getData(): mixed;

    /**
     * Convert a response to a return standard return array.
     *
     * @return array
     *   Structured array or response and http info.
     * @phpstan-return array{
     *     mixed,
     *     int,
     *     array<array<string>>
     *   }
     * @throws \Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException
     *   Thrown if there is a problem decoding or deserializing the response.
     * @deprecated Old style structured array.
     */
    public function toArray(): array;
}
