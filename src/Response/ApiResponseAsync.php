<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Response;

use GuzzleHttp\Promise\PromiseInterface;
use Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * A synchronous API response.
 */
class ApiResponseAsync extends ApiResponse implements ApiResponseInterface
{
    private ResponseInterface $response;

    /**
     * Create an asynchronous API response.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     * *   Related request.
     * @param \GuzzleHttp\Promise\PromiseInterface $promise
     *   Guzzle promise for performing an async request.
     * @param \Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface $deserializer
     *   Deserialization service.
     * @param \Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap $returnTypes
     *   Return types.
     */
    public function __construct(
        RequestInterface $request,
        private readonly PromiseInterface $promise,
        DeserializerInterface $deserializer,
        ResponseTypeMap $returnTypes
    ) {
        parent::__construct($request, $deserializer, $returnTypes);
    }

    /**
     * {@inheritDoc}
     */
    public function getResponse(): ResponseInterface
    {
        if (!isset($this->response)) {
            $this->response = $this->promise->wait();
        }
        return $this->response;
    }

    /**
     * Get the promise related to the request.
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     *   The request promise.
     */
    public function getPromise(): PromiseInterface
    {
        return $this->promise;
    }
}
