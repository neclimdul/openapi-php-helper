<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Response;

use Neclimdul\OpenapiPhp\Helper\HeaderSelector;

/**
 * Class ResponseTypeMap
 *
 * This class is responsible for mapping HTTP response codes and content types to their respective schemas.
 */
class ResponseTypeMap
{
    private HeaderSelector $headerSelector;

    /**
     * ResponseMap constructor.
     *
     * Note: The status codes should be strings but hinting that confuses phpstan.
     *
     * @param array<array-key,array<string,array{type: class-string|string|null}>> $schema_map
     *   The response schemas keyed by response code and content type.
     */
    public function __construct(private array $schema_map)
    {
        $this->headerSelector = new HeaderSelector();
    }

    /**
     * Sets the schema for a given response code and content type.
     *
     * @param string $status_code
     *   HTTP response status code.
     * @param string $content_type
     *   The content type for the response.
     * @param array{type: class-string|string|null} $schema
     *   Schema object.
     */
    public function setSchema(string $status_code, string $content_type, array $schema): void
    {
        if (!isset($this->schema_map[$status_code])) {
            $this->schema_map[$status_code] = [];
        }
        $this->schema_map[$status_code][$content_type] = $schema;
    }

    /**
     * Gets the schema for a given response code and content type.
     *
     * @param string $status_code
     *   HTTP response status code.
     * @param string $content_type
     *   The content type for the response.
     * @return array{type: class-string|string|null}|null
     *   The schema object or null if not found.
     */
    public function getSchema(string $status_code, string $content_type): array|null
    {
        $content_type = $this->headerSelector->selectContentHeader([$content_type]);
        if (isset($this->schema_map[$status_code][$content_type])) {
            $schema = $this->schema_map[$status_code][$content_type];
        }
        if (!isset($schema) && isset($this->schema_map[$status_code]['*/*'])) {
            $schema = $this->schema_map[$status_code]['*/*'];
        }
        return $schema ?? null;
    }

    /**
     * Gets all response codes present in the map.
     *
     * @return string[]
     *   List of response codes.
     */
    public function getResponseCodes(): array
    {
        return array_keys($this->schema_map);
    }

    /**
     * Gets all content types for a given response code.
     *
     * @param string $status_code
     *   HTTP response status code.
     * @return string[]
     *   List of content types or empty array if not found.
     */
    public function getContentTypes(string $status_code): array
    {
        if (isset($this->schema_map[$status_code])) {
            return array_keys($this->schema_map[$status_code]);
        }
        return [];
    }

    /**
     * Get the return type for a response.
     *
     * @param int|string $status_code
     *   HTTP response status code.
     * @param string $content_type
     *   The content type for the response.
     * @return class-string|string|null
     */
    public function getResponseType(int|string $status_code, string $content_type): ?string
    {
        $status_code = (string) $status_code;
        $schema = $this->getSchema($status_code, $content_type);
        if (!isset($schema)) {
            $schema = $this->getSchema('default', $content_type);
        }
        return isset($schema) ? $schema['type'] : null;
    }
}
