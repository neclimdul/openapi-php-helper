<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Response;

use Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * An API response.
 */
abstract class ApiResponse implements ApiResponseInterface
{
    /**
     * Create API response.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     *   Related request.
     * @param \Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface $deserializer
     *   Deserialization service.
     * @param \Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap $returnTypes
     * *   Return types.
     */
    public function __construct(
        protected readonly RequestInterface $request,
        private readonly DeserializerInterface $deserializer,
        private readonly ResponseTypeMap $returnTypes,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    abstract public function getResponse(): ResponseInterface;

    /**
     * {@inheritDoc}
     */
    public function getData(): mixed
    {
        $response = $this->getResponse();
        $returnType = $this->returnTypes->getResponseType(
            $response->getStatusCode(),
            $response->getHeader('Content-Type')[0] ?? '*/*'
        );
        if ($returnType === null) {
            return null;
        }

        $response = $this->getResponse();
        return $this->deserializer->deserialize(
            $response->getBody(),
            $returnType,
            $response->getHeaders(),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    /**
     * {@inheritDoc}
     */
    public function isSuccess(): bool
    {
        return $this->getResponse()->getStatusCode() < 300;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        $response = $this->getResponse();
        // if (!isset($response)) {
        //     return [null, 0, []];
        // }
        return [
            $this->getData(),
            $response->getStatusCode(),
            $response->getHeaders()
        ];
    }
}
