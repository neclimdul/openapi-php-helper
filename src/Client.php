<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseSync;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseAsync;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface;
use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Wrapper around the PSR HTTP client to handle deserialization.
 */
class Client
{
    /**
     * @param \Psr\Http\Client\ClientInterface $client
     *   HTTP Client that will handle making the requests.
     * @param \Neclimdul\OpenapiPhp\Helper\Serialization\DeserializerInterface $deserializer
     *   Response deserializer service.
     */
    public function __construct(
        private readonly ClientInterface $client,
        private readonly DeserializerInterface $deserializer,
    ) {
    }

    /**
     * Make a request.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     *   An initialized request object.
     * @param \Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap $types
     *   Return types.
     * @param callable|null $exception_handler
     *   Additional exception handler if needed to modify exceptions.
     * @param bool $async
     *   If true, request should be made asynchronously.
     *
     * @return \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface
     *   Response of the request.
     * @phpstan-return ($async is true ?
     *      \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseAsync :
     *      \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface
     * )
     * @throws \Psr\Http\Client\ClientExceptionInterface
     *   On error communicating with HTTP server.
     */
    public function makeRequest(
        RequestInterface $request,
        ResponseTypeMap $types,
        ?callable $exception_handler = null,
        bool $async = false,
    ): ApiResponseInterface {
        if (!isset($exception_handler)) {
            $exception_handler = fn(ClientExceptionInterface $e): ClientExceptionInterface => $e;
        }
        if ($async) {
            return $this->doAsyncRequest($request, $types, $exception_handler);
        }

        try {
            $response = $this->client->sendRequest($request);
            return new ApiResponseSync(
                $request,
                $response,
                $this->deserializer,
                $types,
            );
        } catch (ClientExceptionInterface $e) {
            throw $exception_handler($e);
        }
    }

    /**
     * Make an async request.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     *   An initialized request object.
     * @param \Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap $types
     *   Return types.
     * @param callable(ClientExceptionInterface): ClientExceptionInterface $exception_handler
     *   A callback to process HTTP errors.
     *
     * @return \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseAsync
     *   A promise that will return a processed response.
     */
    public function doAsyncRequest(
        RequestInterface $request,
        ResponseTypeMap $types,
        callable $exception_handler,
    ): ApiResponseAsync {
        assert($this->client instanceof GuzzleClientInterface);
        return new ApiResponseAsync(
            $request,
            $this->client->sendAsync($request)->then(
                null,
                function (ClientExceptionInterface $exception) use ($exception_handler): never {
                    throw $exception_handler($exception);
                },
            ),
            $this->deserializer,
            $types,
        );
    }
}
