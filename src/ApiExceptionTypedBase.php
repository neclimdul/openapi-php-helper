<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

/**
 * @deprecated
 * @psalm-suppress DeprecatedInterface
 * @psalm-suppress DeprecatedClass
 */
abstract class ApiExceptionTypedBase extends ApiExceptionBase
{
    /**
     * {@inheritDoc}
     */
    public function getResponseHeaders(): array
    {
        return parent::getResponseHeaders();
    }

    /**
     * {@inheritDoc}
     */
    public function getResponseBody(): ?string
    {
        return parent::getResponseBody();
    }
}
