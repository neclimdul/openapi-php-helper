<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Logging;

use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Error helper class to help with handling and logging errors.
 */
class Error
{
    public const DEFAULT_ERROR_MESSAGE = '!label: "!method !url" resulted in a "!status_code !reason" response';

    /**
     * Log an error response from a request.
     *
     * Creates a log message similar to previous guzzle exception messages.
     *
     * @param \Psr\Log\LoggerInterface $logger
     *   Logger that will log the error.
     * @param \Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface $apiResponse
     *   Response triggering the error.
     * @param string $level
     *   Error level.
     * @param string $message
     *   Error message.
     */
    public static function logError(
        LoggerInterface $logger,
        ApiResponseInterface $apiResponse,
        string $level = LogLevel::ERROR,
        string $message = self::DEFAULT_ERROR_MESSAGE
    ): void {
        $response = $apiResponse->getResponse();
        $request = $apiResponse->getRequest();
        $label = match ((int) floor($response->getStatusCode() / 100)) {
            4 => 'Client error',
            5 => 'Server error',
            default => 'Unsuccessful request',
        };

        $uri = self::sanitizeUri($request->getUri());
        $logger->log(
            $level,
            strtr(
                $message,
                [
                    '!label' => $label,
                    '!method' => $request->getMethod(),
                    '!url' => $uri->__toString(),
                    '!status_code' => $response->getStatusCode(),
                    '!reason' => $response->getReasonPhrase(),
                    '!body' => $response->getBody(),
                ],
            ),
        );
    }

    /**
     * Clean any known private information from URI.
     */
    private static function sanitizeUri(UriInterface $uri): UriInterface
    {
        $info = $uri->getUserInfo();
        $position = strpos($info, ':');
        if ($position !== false) {
            return $uri->withUserInfo(substr($info, 0, $position), '*****');
        }
        return $uri;
    }
}
