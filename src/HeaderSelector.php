<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

/**
 * Class for resolving headers.
 *
 * Contents of the file originally authored by the Swagger and OpenAPI codegen
 * teams.
 */
class HeaderSelector
{
    public const JSON_REGEX = '~(?i)^(application/json|[^;/ \t]+/[^;/ \t]+[+]json)[ \t]*(;.*)?$~';

    /**
     * Format possible headers into expected list of headers.
     *
     * @param string[] $accept
     *   List of accept headers.
     * @param string[] $contentType
     *   List of content type headers.
     * @return array<string, string>
     *   Request headers.
     */
    public function selectHeaders(array $accept, array $contentType): array
    {
        return $this->doSelect($accept, $contentType);
    }

    /**
     * Format possible headers into expected list of headers for form request.
     *
     * @param string[] $accept
     *   List of accept headers.
     * @param string[] $contentType
     *   List of content type headers.
     * @return array<string, string>
     *   Request headers.
     */
    public function selectHeadersForMultipart(array $accept, array $contentType = ['multipart/form-data']): array
    {
        return $this->doSelect($accept, $contentType, 'multipart/form-data');
    }

    /**
     * Build an accept header.
     *
     * @param string[] $accept
     *   List of accept headers.
     * @return string|null
     *   Accept header or null if none.
     */
    public function selectAcceptHeader(array $accept): ?string
    {
        if (!empty($accept) && (count($accept) !== 1 || $accept[0] !== '')) {
            if ($jsonAccept = preg_grep(self::JSON_REGEX, $accept)) {
                return implode(',', $jsonAccept);
            } else {
                return implode(',', $accept);
            }
        }
        return null;
    }

    /**
     * Build a content type header.
     *
     * @param string[] $contentType
     *   List of content type headers.
     * @param string $defaultContentType
     *   Default expected content type.
     * @return string
     *   Content type header.
     */
    public function selectContentHeader(array $contentType, string $defaultContentType = 'application/json'): string
    {
        if (count($contentType) === 0 || (count($contentType) === 1 && $contentType[0] === '')) {
            return $defaultContentType;
        } elseif (preg_grep('/application\/json/i', $contentType)) {
            return 'application/json';
        }
        // TODO this doesn't really make sense. The content type of the
        //  request can only be one thing...
        return implode(',', $contentType);
    }

    /**
     * Handle the logic of selecting headers.
     *
     * @param string[] $accept
     *   List of accept headers.
     * @param string[] $contentType
     *   List of content type headers.
     * @param string $defaultContentType
     *   Default expected content type.
     * @return array<string, string>
     *   Request headers.
     */
    private function doSelect(array $accept, array $contentType, string $defaultContentType = 'application/json'): array
    {
        $headers = [];

        $accepts = $this->selectAcceptHeader($accept);
        /** @psalm-suppress RiskyTruthyFalsyComparison */
        if (!empty($accepts)) {
            $headers['Accept'] = $accepts;
        }

        $headers['Content-Type'] = $this->selectContentHeader($contentType, $defaultContentType);

        return $headers;
    }
}
