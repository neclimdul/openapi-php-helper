<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

/**
 * Interface abstracting model access.
 */
interface ModelTypedInterface extends ModelInterface
{
    /**
     * {@inheritDoc}
     */
    public function getModelName(): string;

    /**
     * {@inheritDoc}
     */
    public static function openAPITypes(): array;

    /**
     * {@inheritDoc}
     */
    public static function openAPIFormats(): array;

    /**
     * {@inheritDoc}
     */
    public static function attributeMap(): array;

    /**
     * {@inheritDoc}
     */
    public static function setters(): array;

    /**
     * {@inheritDoc}
     */
    public static function getters(): array;
}
