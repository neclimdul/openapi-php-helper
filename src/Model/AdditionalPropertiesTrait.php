<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

/**
 * Additional property implementation trait.
 */
trait AdditionalPropertiesTrait
{
    /**
     * Associative array for storing additional property values.
     *
     * @var array<string,mixed>
     */
    private array $additional = [];

    public function setAdditionalProperties(array $fields): void
    {
        foreach ($fields as $k => $v) {
            assert(!isset(static::$attributeMap[$k]));
            $this->additional[$k] = $v;
        }
    }

    public function getAdditionalProperties(): array
    {
        return $this->additional;
    }

    public function setAdditionalProperty(string $property, mixed $value): void
    {
        assert(!isset(static::$attributeMap[$property]));
        $this->additional[$property] = $value;
    }

    public function getAdditionalProperty(string $property): mixed
    {
        return $this->additional[$property] ?? null;
    }

    public function hasAdditionalProperty(string $property): bool
    {
        return isset($this->additional[$property]);
    }
}
