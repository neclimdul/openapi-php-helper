<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

interface ValidationInterface
{
    /**
     * Validate all the properties in the model.
     *
     * @return bool
     *   True all properties pass.
     */
    public function valid(): bool;

    /**
     * Show all the invalid properties with reasons.
     *
     * @return string[]
     */
    public function listInvalidProperties(): array;
}
