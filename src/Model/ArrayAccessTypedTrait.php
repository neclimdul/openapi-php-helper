<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

/**
 * @template TKey
 * @template TValue
 * @property array<TKey, mixed> $container
 * @property array<TKey, mixed> $additional
 */
trait ArrayAccessTypedTrait
{
    /**
     * @param TKey $offset
     * @return bool
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->container[$offset]) ||
            ($this instanceof AdditionalPropertiesInterface && $this->hasAdditionalProperty($offset));
    }

    /**
     * @param TKey $offset
     * @return TValue|null
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->container[$offset] ??
            ($this instanceof AdditionalPropertiesInterface && $this->getAdditionalProperty($offset));
    }

    /**
     * @param TKey|null $offset
     * @param TValue $value
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if (is_null($offset)) {
            throw new \RuntimeException(
                'Appending to a model does not make sense. Provide an explicit property instead.'
            );
        } elseif (isset(static::$attributeMap[$offset])) {
            $this->container[$offset] = $value;
        } elseif ($this instanceof AdditionalPropertiesInterface) {
            $this->setAdditionalProperty($offset, $value);
        } else {
            throw new \InvalidArgumentException('Bad property.');
        }
    }

    /**
     * @param TKey $offset
     * @return void
     */
    public function offsetUnset(mixed $offset): void
    {
        /** @psalm-suppress PossiblyNullArrayOffset */
        unset($this->container[$offset]);
        if (!isset(static::$attributeMap[$offset])) {
            $this instanceof AdditionalPropertiesInterface && $this->setAdditionalProperty($offset, null);
        }
    }
}
