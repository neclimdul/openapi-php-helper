<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

/**
 * Interface abstracting model access.
 */
interface ModelInterface
{
    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName();

    /**
     * Array of property to type mappings.
     *
     * Used for (de)serialization.
     *
     * @return array<string, string|class-string>
     */
    public static function openAPITypes();

    /**
     * Array of property to format mappings.
     *
     * Used for (de)serialization.
     *
     * @return array<string, string|null>
     */
    public static function openAPIFormats();

    /**
     * Array of attributes.
     *
     * Key is the local name, and the value is the original name.
     *
     * @return array<array-key, string>
     */
    public static function attributeMap();

    /**
     * Array of attributes to setter functions.
     *
     * For deserialization of responses.
     *
     * @return array<array-key, string>
     */
    public static function setters();

    /**
     * Array of attributes to getter functions.
     *
     * For serialization of requests.
     *
     * @return array<array-key, string>
     */
    public static function getters();
}
