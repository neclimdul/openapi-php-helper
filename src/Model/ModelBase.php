<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

abstract class ModelBase implements ModelInterface
{
    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $openAPIModelName;

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|class-string>
     */
    protected static $openAPITypes = [];

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @var string[]
     * @phpstan-var array<string, string|null>
     */
    protected static $openAPIFormats = [];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [];

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * {@inheritDoc}
     */
    public function getModelName()
    {
        return static::$openAPIModelName;
    }

    /**
     * {@inheritDoc}
     */
    public static function openAPITypes()
    {
        return static::$openAPITypes;
    }

    /**
     * {@inheritDoc}
     */
    public static function openAPIFormats()
    {
        return static::$openAPIFormats;
    }

    /**
     * {@inheritDoc}
     */
    public static function attributeMap()
    {
        return static::$attributeMap;
    }

    /**
     * {@inheritDoc}
     */
    public static function setters()
    {
        return static::$setters;
    }

    /**
     * {@inheritDoc}
     */
    public static function getters()
    {
        return static::$getters;
    }

    /**
     * Collect data for an allowed values.
     *
     * @param array<string, mixed> $data
     *   Raw data to populate a model with.
     * @return array<string, mixed>
     *   Allowed data.
     */
    protected function collectAttributeValues(array $data): array
    {
        return array_merge(
            array_fill_keys(array_keys(static::$attributeMap), null),
            array_intersect_key($data, static::$attributeMap)
        );
    }
}
