<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

/**
 * @property array<string, mixed> $container
 * @property array<string, mixed> $additional
 * @template TKey of array-key
 * @template TValue
 */
trait ArrayAccessTrait
{
    /**
     * @param TKey $offset
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]) ||
            ($this instanceof AdditionalPropertiesInterface && $this->hasAdditionalProperty($offset));
    }

    /**
     * @param TKey $offset
     * @return TValue|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ??
            ($this instanceof AdditionalPropertiesInterface && $this->getAdditionalProperty($offset));
    }

    /**
     * @param TKey|null $offset
     * @param TValue $value
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            throw new \RuntimeException(
                'Appending to a model does not make sense. Provide an explicit property instead.'
            );
        } elseif (isset(static::$attributeMap[$offset])) {
            $this->container[$offset] = $value;
        } elseif ($this instanceof AdditionalPropertiesInterface) {
            $this->setAdditionalProperty($offset, $value);
        } else {
            throw new \InvalidArgumentException('Bad property.');
        }
    }

    /**
     * @param TKey $offset
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset): void
    {
        /** @psalm-suppress PossiblyNullArrayOffset */
        unset($this->container[$offset]);
        if (!isset(static::$attributeMap[$offset])) {
            $this instanceof AdditionalPropertiesInterface && $this->setAdditionalProperty($offset, null);
        }
    }
}
