<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

interface AdditionalPropertiesInterface
{
    /**
     * Set list of additional properties for the model.
     *
     * @param array<string,mixed> $fields
     *   List of additional properties
     */
    public function setAdditionalProperties(array $fields);

    /**
     * Get list of additional properties for the model.
     *
     * @return array<string,mixed>
     *   List of additional properties
     */
    public function getAdditionalProperties();

    /**
     * Set an additional property for the model.
     *
     * @param string $property
     *   Property name.
     * @param mixed $value
     *   Value of the property.
     */
    public function setAdditionalProperty(string $property, mixed $value);

    /**
     * Get a specific additional property.
     *
     * @param string $property
     *   The property key.
     * @return mixed
     *   The property value.
     */
    public function getAdditionalProperty(string $property);

    /**
     * Check if a specific additional property is set.
     *
     * @param string $property
     *   The property key.
     * @return bool
     *   True if the property have been set.
     */
    public function hasAdditionalProperty(string $property): bool;
}
