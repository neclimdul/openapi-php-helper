<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Model;

interface AdditionalPropertiesTypedInterface extends AdditionalPropertiesInterface
{
    /**
     * {@inheritDoc}
     * @phpstan-return array<string,mixed>
     */
    public function setAdditionalProperties(array $fields): array;

    /**
     * {@inheritDoc}
     */
    public function setAdditionalProperty(string $property, mixed $value): void;

    /**
     * {@inheritDoc}
     */
    public function getAdditionalProperties(): array;
}
