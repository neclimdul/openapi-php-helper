<?php

/**
 * Base configuration class.
 *
 * Contents of the file originally authored by the Swagger and OpenAPI codegen
 * teams.
 */

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

/**
 * Configuration Class Doc Comment
 */
abstract class Configuration
{
    /**
     * Associate array to store API key(s).
     *
     * @var string[]
     */
    protected array $apiKeys = [];

    /**
     * Associate array to store API prefix (e.g. Bearer).
     *
     * @var string[]
     */
    protected array $apiKeyPrefixes = [];

    /**
     * Access token for OAuth/Bearer authentication.
     */
    protected string $accessToken = '';

    /**
     * Username for HTTP basic authentication.
     */
    protected string $username = '';

    /**
     * Password for HTTP basic authentication.
     */
    protected string $password = '';

    /**
     * Debug switch.
     */
    protected bool $debug = false;

    /**
     * Debug file location. Log to STDOUT by default.
     */
    protected string $debugFile = 'php://output';

    /**
     * Debug file location Log to STDOUT by default.
     */
    protected string $tempFolderPath;

    /**
     * Construct a Configuration object.
     *
     * @param string $host
     *   The current API host.
     * @param string $userAgent
     *   User agent of the HTTP request.
     */
    public function __construct(
        protected string $host,
        protected string $userAgent
    ) {
        $this->tempFolderPath = sys_get_temp_dir();
    }

    /**
     * Sets the current API host.
     *
     * @param string $host
     *   Current PAI host.
     *
     * @return $this
     */
    public function setHost(string $host): Configuration
    {
        $this->host = $host;
        return $this;
    }

    /**
     * Gets the current API host.
     *
     * @return string
     *   The current host.
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Sets the user agent of the api client.
     *
     * @param string $userAgent
     *   The user agent of the api client.
     *
     * @return $this
     */
    public function setUserAgent(string $userAgent): Configuration
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * Gets the user agent of the api client.
     *
     * @return string
     *   The current user agent.
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * Sets the API key.
     *
     * @param string $apiKeyIdentifier
     *   API key identifier (authentication scheme).
     * @param string $key
     *   API key or token.
     *
     * @return $this
     */
    public function setApiKey(string $apiKeyIdentifier, string $key): Configuration
    {
        $this->apiKeys[$apiKeyIdentifier] = $key;
        return $this;
    }

    /**
     * Gets API key.
     *
     * @param string $apiKeyIdentifier
     *   API key identifier (authentication scheme).
     *
     * @return string|null
     *   API key or token.
     */
    public function getApiKey(string $apiKeyIdentifier): ?string
    {
        return $this->apiKeys[$apiKeyIdentifier] ?? null;
    }

    /**
     * Sets the prefix for API key (e.g. Bearer).
     *
     * @param string $apiKeyIdentifier
     *   API key identifier (authentication scheme).
     * @param string $prefix
     *   API key prefix, e.g. Bearer.
     *
     * @return $this
     */
    public function setApiKeyPrefix(string $apiKeyIdentifier, string $prefix): Configuration
    {
        $this->apiKeyPrefixes[$apiKeyIdentifier] = $prefix;
        return $this;
    }

    /**
     * Gets API key prefix.
     *
     * @param string $apiKeyIdentifier
     *   API key identifier (authentication scheme).
     */
    public function getApiKeyPrefix(string $apiKeyIdentifier): ?string
    {
        return $this->apiKeyPrefixes[$apiKeyIdentifier] ?? null;
    }

    /**
     * Get API key (with prefix if set).
     *
     * @param string $apiKeyIdentifier
     *   Name of apikey.
     *
     * @return string|null
     *   API key with the prefix.
     */
    public function getApiKeyWithPrefix(string $apiKeyIdentifier): ?string
    {
        $prefix = $this->getApiKeyPrefix($apiKeyIdentifier);
        $apiKey = $this->getApiKey($apiKeyIdentifier);

        if ($apiKey === null) {
            return null;
        }

        if ($prefix === null) {
            $keyWithPrefix = $apiKey;
        } else {
            $keyWithPrefix = $prefix . ' ' . $apiKey;
        }

        return $keyWithPrefix;
    }

    /**
     * Sets the access token for OAuth.
     *
     * @param string $accessToken
     *   Token for OAuth.
     *
     * @return $this
     */
    public function setAccessToken(string $accessToken): Configuration
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * Gets the access token for OAuth.
     *
     * @return string
     *   Access token for OAuth.
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * Sets the username for HTTP basic authentication.
     *
     * @param string $username
     *   Username for HTTP basic authentication.
     *
     * @return $this
     */
    public function setUsername(string $username): Configuration
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Gets the username for HTTP basic authentication.
     *
     * @return string
     *   Username for HTTP basic authentication.
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Sets the password for HTTP basic authentication.
     *
     * @param string $password
     *   Password for HTTP basic authentication.
     *
     * @return $this
     */
    public function setPassword(string $password): Configuration
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Gets the password for HTTP basic authentication.
     *
     * @return string
     *   Password for HTTP basic authentication.
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Sets debug flag.
     *
     * @param bool $debug
     *   Debug flag.
     *-
     * @return $this
     */
    public function setDebug(bool $debug): Configuration
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * Gets the debug flag.
     */
    public function getDebug(): bool
    {
        return $this->debug;
    }

    /**
     * Sets the debug file.
     *
     * @param string $debugFile
     *   Debug file.
     *
     * @return $this
     */
    public function setDebugFile(string $debugFile): Configuration
    {
        $this->debugFile = $debugFile;
        return $this;
    }

    /**
     * Gets the debug file.
     */
    public function getDebugFile(): string
    {
        return $this->debugFile;
    }

    /**
     * Sets the temp folder path.
     *
     * @param string $tempFolderPath
     *   Temp folder path.
     *
     * @return $this
     */
    public function setTempFolderPath(string $tempFolderPath): Configuration
    {
        $this->tempFolderPath = $tempFolderPath;
        return $this;
    }

    /**
     * Gets the temp folder path.
     *
     * @return string
     *   Temp folder path.
     */
    public function getTempFolderPath(): string
    {
        return $this->tempFolderPath;
    }

    /**
     * Returns an array of host settings
     *
     * @return array<array-key, array{
     *     'url': string,
     *     'description': string,
     *     'variables'?: array<string, array{
     *       'enum_values'?: array<array-key, string|numeric>,
     *       'default_value': string|numeric
     *     }>
     *   }>
     *   An array of host settings.
     */
    abstract public function getHostSettings(): array;

    /**
     * Returns URL based on the index and variables
     *
     * @param int        $index     index of the host settings
     * @param array<string, scalar>|null $variables hash of variable and the corresponding value (optional)
     * @return string URL based on host settings
     */
    public function getHostFromSettings(int $index, ?array $variables = null): string
    {
        if (null === $variables) {
            $variables = [];
        }

        $hosts = $this->getHostSettings();

        // check array index out of bound
        if ($index < 0 || $index >= sizeof($hosts)) {
            throw new \InvalidArgumentException(
                "Invalid index $index when selecting the host. Must be less than " . sizeof($hosts)
            );
        }

        $host = $hosts[$index];
        $url = $host['url'];

        // go through variable and assign a value
        foreach ($host['variables'] ?? [] as $name => $variable) {
            // check to see if it's in the variables provided by the user.
            if (isset($variables[$name])) {
                if (isset($variable['enum_values'])) {
                    if (in_array($variables[$name], $variable['enum_values'], true)) {
                        // check to see if the value is in the enum
                        $url = str_replace('{' . $name . '}', (string) $variables[$name], $url);
                    } else {
                        throw new \InvalidArgumentException(
                            "The variable '$name' in the host URL has invalid value " . var_export(
                                $variables[$name],
                                true
                            ) . '. ' . 'Must be ' . join(',', $variable['enum_values']) . '.'
                        );
                    }
                } else {
                    $url = str_replace('{' . $name . '}', (string) $variables[$name], $url);
                }
            } else {
                // use default value
                $url = str_replace('{' . $name . '}', (string) $variable['default_value'], $url);
            }
        }

        return $url;
    }
}
