<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Query;
use GuzzleHttp\Psr7\Utils;
use Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @template TModelInterface of \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
 */
class RequestFactory
{
    /**
     * @param \Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface<TModelInterface> $serializer
     */
    public function __construct(
        private readonly RequestFactoryInterface $requestFactory,
        private readonly SerializerInterface $serializer,
    ) {
    }

    /**
     * Create a request from relevant values.
     *
     * @param string $method
     *   Request method type.
     * @param string $uri
     *   Request uri.
     * @param array<array-key, mixed> $query
     *   Query parameters.
     * @param array<string, string|null> $headers
     *   Request headers.
     * @param array<string|resource[]|scalar[]|null> $form
     *   Form parameters.
     * @param mixed $body
     *   Body object.
     */
    public function createRequest(
        string $method,
        string $uri,
        array $query,
        array $headers,
        array $form,
        mixed $body
    ): RequestInterface {
        $request = $this->requestFactory->createRequest(
            $method,
            $uri
        );
        $headers = array_filter($headers, fn($v) => $v !== null);
        foreach ($headers as $k => $v) {
            $request = $request->withHeader($k, $v);
        }

        $request = $request->withUri(
            $request->getUri()->withQuery(
                Query::build(
                    array_filter($query, fn($v) => $v !== null)
                )
            )
        );

        $form = array_filter($form, fn($v) => $v !== null);
        $content_type = ($headers['Content-Type'] ?? '');
        assert(
            empty($form) || empty($body),
            'Form parameters will override body.'
        );
        // @todo probably need to fix the content type if we detected the need
        //   to submit a multipart form.
        if (!empty($form)) {
            // This is a bit hacky but sometimes APIs aren't great about reporting
            // they need to post multipart data. This detects any file parameters
            // and forces a multipart submission.
            $multipart = $content_type === 'multipart/form-data' || array_reduce(
                $form,
                fn(?bool $carry, $items): bool => $carry
                    || is_array($items)
                        && array_reduce(
                            $items,
                            fn(?bool $carry, $item): bool => $carry || is_resource($item)
                        ),
                false
            );
            if ($multipart) {
                $request = $request->withBody(
                    $this->createMultipartStream($form)
                );
            } else {
                $request = $request->withBody(
                    Utils::streamFor(Query::build($form))
                );
            }
        } else {
            $request = $request->withBody(Utils::streamFor(
                !empty($body) ?
                    $this->serializer->toBodyValue($body, $content_type) :
                    ''
            ));
        }

        return $request;
    }

    /**
     * Convert structured multipart data into Guzzle compatible multipart data.
     *
     * @param array<string, string|mixed[]> $form
     *   Multipart form data.
     *
     * @return \Psr\Http\Message\StreamInterface
     *   A stream containing the multipart data ready for the request.
     */
    private function createMultipartStream(array $form): StreamInterface
    {
        $multipartContents = [];
        foreach ($form as $paramName => $paramValue) {
            if (is_array($paramValue)) {
                foreach ($paramValue as $paramValueItem) {
                    $multipartContents[] = [
                        'name' => $paramName,
                        'contents' => $paramValueItem,
                    ];
                }
            } else {
                $multipartContents[] = [
                    'name' => $paramName,
                    'contents' => $paramValue,
                ];
            }
        }

        // for HTTP post (form)
        return new MultipartStream($multipartContents);
    }
}
