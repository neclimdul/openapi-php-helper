<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

/**
 * Exception interface for API requests using the OpenApi helper library.
 *
 * @deprecated Use new Exception\ApiExceptionInterface
 */
interface ApiExceptionInterface extends \Throwable
{
    /**
     * Gets the HTTP response header.
     *
     * @return array<string, string[]>
     *   HTTP response header.
     */
    public function getResponseHeaders();

    /**
     * Gets the HTTP body of the server response either as a string.
     *
     * @return string|null
     *   HTTP body of the server response either as \stdClass or string.
     */
    public function getResponseBody();

    /**
     * Sets the deserialized response object (during deserialization).
     *
     * @param mixed $obj
     *   Deserialized response object.
     *
     * @return void
     */
    public function setResponseObject(mixed $obj);

    /**
     * Gets the deserialized response object (during deserialization).
     *
     * @return mixed
     *   The deserialized response object.
     */
    public function getResponseObject();
}
