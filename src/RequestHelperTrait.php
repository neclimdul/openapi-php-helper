<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseSync;
use Neclimdul\OpenapiPhp\Helper\Response\ResponseTypeMap;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @deprecated Use of this trait should be converted to RequestFactory.
 *
 * @template TException of \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
 * @template TModelInterface of \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
 */
trait RequestHelperTrait
{
    /** @use \Neclimdul\OpenapiPhp\Helper\CreateRequestTrait<TException,TModelInterface> */
    use CreateRequestTrait {
        setSerializerForRequest as setSerializerForRequestTrait;
    }

    /**
     * @var \Neclimdul\OpenapiPhp\Helper\ObjectSerializer<TException, TModelInterface>
     */
    private ObjectSerializer $serializer;

    /**
     * @var class-string<TException>
     */
    private string $exception;

    /**
     * @var \GuzzleHttp\ClientInterface&\Psr\Http\Client\ClientInterface
     */
    private ClientInterface $client;

    /**
     * @var \Neclimdul\OpenapiPhp\Helper\Client
     */
    private Client $apiClient;

    /**
     * Set the serializer to use for creating the request.
     *
     * @param \Neclimdul\OpenapiPhp\Helper\ObjectSerializer<TException, TModelInterface> $serializer
     *   An instance of an object serializer.
     */
    private function setSerializerForRequest(ObjectSerializer $serializer): void
    {
        $this->serializer = $serializer;
        $this->setSerializerForRequestTrait($serializer);
    }

    /**
     * Set the Guzzle client for making requests.
     *
     * @param \GuzzleHttp\ClientInterface&\Psr\Http\Client\ClientInterface $client
     *   Guzzle/PSR Client.
     */
    private function setClientForRequest(ClientInterface $client): void
    {
        $this->client = $client;
    }

    /**
     * Set the configuration to use for creating the request.
     *
     * @param \Neclimdul\OpenapiPhp\Helper\Configuration $configuration
     *   An instance of a configuration object.
     */
    private function setConfigForRequest(Configuration $configuration): void
    {
    }

    /**
     * Set the exception to throw when making requests.
     *
     * @param class-string<TException> $exception
     */
    private function setExceptionForRequest(string $exception): void
    {
        assert(is_subclass_of($exception, ApiExceptionInterface::class));
        $this->exception = $exception;
    }

    /**
     * @return \Neclimdul\OpenapiPhp\Helper\Client
     */
    private function getOpenApiClient(): Client
    {
        if (!isset($this->apiClient)) {
            $this->apiClient = (new Client(
                $this->client,
                $this->serializer->getDeserializer(),
            ));
        }
        return $this->apiClient;
    }

    /**
     * Make a request.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     *   An initialized request object.
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
     *   On non-2xx response.
     */
    protected function makeRequest(RequestInterface $request): ResponseInterface
    {
        // Note: the passed type doesn't matter because we're just returning
        //   the raw response.
        try {
            $response = $this
                ->getOpenApiClient()
                ->makeRequest($request, new ResponseTypeMap([]))
                ->getResponse();

            $statusCode = $response->getStatusCode();
            if ($statusCode < 200 || $statusCode > 299) {
                throw new $this->exception(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        (string)$request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    (string)$response->getBody()
                );
            }
            return $response;
        } catch (ClientExceptionInterface $e) {
            throw $this->generateExceptionFromException($e);
        }
    }

    /**
     * Make an async request.
     *
     * @param \Psr\Http\Message\RequestInterface $request
     *   An initialized request object.
     * @param callable $exceptionHandler
     *   A callback to process HTTP errors.
     *
     * @return \GuzzleHttp\Promise\PromiseInterface
     *   A promise that will return a processed response.
     * @throws \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
     *   On non-2xx response.
     */
    protected function makeAsyncRequest(
        RequestInterface $request,
        callable $exceptionHandler
    ): PromiseInterface {
        $exceptionHandler = fn(ClientExceptionInterface $e) => $exceptionHandler(
            $this->generateExceptionFromException($e),
        );
        // Note: the passed type doesn't matter because we're just returning
        //   the Promise.
        return $this->getOpenApiClient()
            ->makeRequest($request, new ResponseTypeMap([]), $exceptionHandler, true)
            ->getPromise()
            ->then(
                null,
                fn(ApiExceptionInterface $e) => throw $e,
            );
    }

    /**
     * Convert a response to a return standard return array.
     *
     * @template T
     * @param \Psr\Http\Message\ResponseInterface $response
     *   A response from a request with a serialized body.
     * @param class-string<T>|string|null $returnType
     *   The primary return type.
     *
     * @return array
     *   Structured array or response and http info.
     * @phpstan-return array{
     *     ($returnType is null ? null : ($returnType is class-string<T> ? T : scalar)),
     *     int,
     *     array<array<string>>
     *   }
     * @throws \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
     *   On non-2xx response.
     * @deprecated
     */
    protected function responseToReturn(
        ResponseInterface $response,
        ?string $returnType
    ): array {
        try {
            return (new ApiResponseSync(
                new Request('GET', '/'),
                $response,
                $this->serializer,
                new ResponseTypeMap(['default' => ['*/*' => ['type' => $returnType]]]),
            ))->toArray();
        } catch (Exception\ApiSerializationException $e) {
            throw new $this->exception(
                $e->getMessage(),
                $e->getCode(),
                $response->getHeaders(),
                $response->getBody(),
                $e,
            );
        }
    }

    /**
     * Exception generator.
     *
     * @param \Psr\Http\Client\ClientExceptionInterface $e
     *   Triggering exception.
     * @return \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
     *   Wrapped API Helper exception.
     */
    private function generateExceptionFromException(
        ClientExceptionInterface $e
    ): ApiExceptionInterface {
        $response = null;
        // Guzzle exceptions have responses we can improve the exception.
        if (method_exists($e, 'getResponse')) {
            /** @var ResponseInterface $response */
            $response = $e->getResponse();
        }
        $request = null;
        // Guzzle exceptions have responses we can improve the exception.
        if (method_exists($e, 'getRequest')) {
            /** @var RequestInterface $request */
            $request = $e->getRequest();
        }
        /** @psalm-suppress RedundantCast */
        $code = $response ? $response->getStatusCode() : (int) $e->getCode();
        return new $this->exception(
            sprintf(
                '[%d] Error connecting to the API (%s)',
                $code,
                (string) $request?->getUri(),
            ),
            $code,
            $response ? $response->getHeaders() : [],
            (string)$response?->getBody(),
            $e,
        );
    }
}
