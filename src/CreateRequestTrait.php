<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper;

use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Request;

/**
 * Trait to help make a Guzzle request for an OpenAPI operation.
 *
 * Contents of the file based on code originally authored by the Swagger and
 * OpenAPI codegen teams.
 *
 * @deprecated Use of this trait should be converted to RequestFactory.
 *
 * @template TException of \Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface
 * @template TModelInterface of \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
 */
trait CreateRequestTrait
{
    /**
     * @var \Neclimdul\OpenapiPhp\Helper\ObjectSerializer<TException,TModelInterface>
     */
    private ObjectSerializer $serializer;

    /**
     * Set the serializer to use for creating the request.
     *
     * @param \Neclimdul\OpenapiPhp\Helper\ObjectSerializer<TException,TModelInterface> $serializer
     *   An instance of an object serializer.
     */
    protected function setSerializerForRequest(ObjectSerializer $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * Create a request from relevant values.
     *
     * @param string $method
     *   Request method type.
     * @param string $uri
     *   Request uri.
     * @param array<array-key, mixed> $query
     *   Query parameters.
     * @param array<string, string|null> $headers
     *   Request headers.
     * @param array<string|resource[]|scalar[]|null> $form
     *   Form parameters.
     * @param mixed $body
     *   Body object.
     *
     * @return \GuzzleHttp\Psr7\Request
     *   A new request.
     */
    public function createRequest(
        string $method,
        string $uri,
        array $query,
        array $headers,
        array $form,
        mixed $body
    ): Request {
        $request = (new RequestFactory(
            new HttpFactory(),
            $this->serializer,
        ))->createRequest(
            $method,
            $uri,
            $query,
            $headers,
            $form,
            $body,
        );
        // This will always be true because we passed the Guzzle HttpFactory.
        assert($request instanceof Request);
        return $request;
    }
}
