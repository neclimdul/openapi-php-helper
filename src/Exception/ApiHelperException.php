<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Exception;

/**
 * OpenAPI exception interface.
 */
interface ApiHelperException extends \Throwable
{
}
