<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Exception;

/**
 * Exception thrown by problems with serialization.
 */
class ApiSerializationException extends \Exception implements ApiHelperException
{
}
