<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Serialization;

use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;

/**
 * Base object serialization class.
 *
 * Contents of the file originally authored by the Swagger and OpenAPI codegen
 * teams.
 *
 * @template TModelInterface of \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
 */
interface SerializerInterface
{
    /**
     * Change the date format
     *
     * @param string $format
     *   The new date format to use.
     */
    public function setDateTimeFormat(string $format): void;

    /**
     * Serialize data.
     *
     * @param mixed $data
     *   The data to serialize.
     * @param ?string $type
     *   The OpenAPIToolsType of the data.
     * @param ?string $format
     *   The format of the OpenAPITools type of the data.
     *
     * @return scalar|object|mixed[]|null serialized form of $data
     */
    public function sanitizeForSerialization(mixed $data, ?string $type = null, ?string $format = null);

    /**
     * Sanitize filename by removing path.
     * e.g. ../../sun.gif becomes sun.gif
     *
     * @param string $filename
     *   The filename to be sanitized.
     *
     * @return string
     *   The sanitized filename.
     */
    public function sanitizeFilename(string $filename): string;

    /**
     * Encode a value to be included in a URI path.
     *
     * @param scalar[]|scalar|\DateTime $value
     *   A value which will be part of the path
     *
     * @return string
     *   The serialized value.
     */
    public function toPathValue($value): string;

    /**
     * Encode a list of values for inclusion in a query.
     *
     * Take value and turn it into a string suitable for inclusion in the query,
     * by imploding comma-separated if it's an object. If it's a string, pass
     * through unchanged. It will be url-encoded later.
     *
     * @param scalar[]|scalar|\DateTime $value
     *   An object to be serialized to a string.
     *
     * @return string
     *   A serialized query parameter. Null to skip.
     */
    public function toQueryValue($value): string;

    /**
     * Process values and prep them for Guzzle query serialization.
     *
     * @param string $name
     *   Query parameter name.
     * @param string $style
     *   Query parameter style.
     * @return array<string, string>
     *   A hash of values keyed by the query parameter name(s).
     */
    public function processQuery(string $name, mixed $value, string $style): array;

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the header. If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     *
     * @param ModelInterface|scalar $value
     *   A string which will be part of the header.
     *
     * @return string
     *   The header value as a string.
     */
    public function toHeaderValue($value): string;

    /**
     * Convert file parameter(s) to format expected by guzzle.
     *
     * @param string|\SplFileObject|array<string|\SplFileObject> $file
     *   The value of the form parameter.
     *
     * @return resource[]
     *   File resources.
     */
    public function fileToFormValue($file): array;

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the http body (form parameter). If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     *
     * @param scalar|\SplFileObject|ModelInterface $value
     *   The value of the form parameter
     *
     * @return string
     *   The form value prepared as a string.
     * @throws \Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException
     *   Thrown if unable to convert into a string.
     */
    public function toFormValue($value): string;

    /**
     * Take value and turn it into a string suitable for inclusion in
     * the parameter. If it's a string, pass through unchanged
     * If it's a datetime object, format it in ISO8601
     * If it's a boolean, convert it to "true" or "false".
     *
     * @param scalar|\DateTime $value
     *   The value of the parameter.
     *
     * @return string
     *   The header value as string.
     */
    public function toString($value): string;

    /**
     * Convert body object into transferable string.
     *
     * @param string $contentType
     *   The expected body content type.
     * @return string|\Psr\Http\Message\StreamInterface
     *   Encoded body value.
     */
    public function toBodyValue(mixed $object, string $contentType);

    /**
     * Serialize an array to a string.
     *
     * @see https://swagger.io/specification/#style-examples
     *
     * @param string|scalar[] $collection
     *   Collection to serialize to a string.
     * @param string $style
     *   The format use for serialization (csv, ssv, tsv, pipes, multi).
     * @param bool $allowCollectionFormatMulti
     *   Allow collection format to be a multidimensional array.
     *
     * @return string
     *   Serialized collection.
     * @throws \Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException
     *   Thrown if php is unable to convert the collection into a string.
     */
    public function serializeCollection($collection, string $style, bool $allowCollectionFormatMulti = false): string;
}
