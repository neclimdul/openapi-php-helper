<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Serialization;

use GuzzleHttp\Psr7\Utils as GuzzleUtils;
use Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException;
use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesInterface;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Base object serialization class.
 *
 * Contents of the file originally authored by the Swagger and OpenAPI codegen
 * teams.
 *
 * @template TModelInterface of \Neclimdul\OpenapiPhp\Helper\Model\ModelInterface
 * @implements \Neclimdul\OpenapiPhp\Helper\Serialization\SerializerInterface<TModelInterface>
 */
class Serializer implements SerializerInterface
{
    private const PRIMITIVES = [
        'bool',
        'boolean',
        'int',
        'integer',
        'double',
        'float',
        'object',
        'array',
        'string',
        'null',
    ];

    /**
     * Construct an ObjectSerializer.
     *
     * @param class-string<TModelInterface> $modelInterface
     *   The model interface used by the library for models.
     * @param string $dateTimeFormat
     *   The format to be used when formatting date time objects.
     */
    public function __construct(
        private readonly string $modelInterface,
        private string $dateTimeFormat = \DateTimeInterface::ATOM
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function setDateTimeFormat(string $format): void
    {
        $this->dateTimeFormat = $format;
    }

    /**
     * {@inheritDoc}
     */
    public function sanitizeForSerialization(
        mixed $data,
        ?string $type = null,
        ?string $format = null
    ): mixed {
        if (is_scalar($data) || null === $data) {
            return $data;
        }

        if ($data instanceof \DateTime) {
            return ($format === 'date') ?
                $data->format('Y-m-d') :
                $data->format($this->dateTimeFormat);
        }

        if (is_array($data)) {
            foreach ($data as $property => $value) {
                $data[$property] = $this->sanitizeForSerialization($value);
            }
            return $data;
        }

        if (is_object($data)) {
            $values = [];
            if (is_subclass_of($data, $this->modelInterface)) {
                $formats = $data::openAPIFormats();
                $getters = $data::getters();
                $map = $data::attributeMap();
                foreach ($data::openAPITypes() as $property => $openAPIType) {
                    $getter = $getters[$property];
                    $value = $data->$getter();
                    if (
                        $value !== null
                        && !in_array($openAPIType, self::PRIMITIVES, true)
                    ) {
                        $callable = [$openAPIType, 'getAllowableEnumValues'];
                        if (is_callable($callable)) {
                            $allowedEnumTypes = $callable();
                            if (!in_array($value, $allowedEnumTypes, true)) {
                                $imploded = implode("', '", $allowedEnumTypes);
                                throw new \InvalidArgumentException(
                                    "Invalid value for enum '$openAPIType', must be one of: '$imploded'"
                                );
                            }
                        }
                    }
                    if ($value !== null) {
                        $values[$map[$property]] = $this->sanitizeForSerialization(
                            $value,
                            $openAPIType,
                            $formats[$property]
                        );
                    }
                }
                if ($data instanceof AdditionalPropertiesInterface) {
                    // TODO These additional properties should probably be "sanitized".
                    $values = array_merge(
                        $values,
                        $data->getAdditionalProperties() ?: []
                    );
                }
            } elseif (is_iterable($data) || $data instanceof \stdClass) {
                foreach ((array)$data as $property => $value) {
                    $values[$property] = $this->sanitizeForSerialization(
                        $value
                    );
                }
            }
            return (object)$values;
        } else {
            return (string)$data;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function sanitizeFilename(string $filename): string
    {
        if (preg_match('/.*[\/\\\](.*)$/', $filename, $match)) {
            return $match[1];
        } else {
            return $filename;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function toPathValue($value): string
    {
        return rawurlencode($this->toQueryValue($value));
    }

    /**
     * {@inheritDoc}
     */
    public function toQueryValue($value): string
    {
        if (is_array($value)) {
            return implode(',', array_map($this->toString(...), $value));
        }
        return $this->toString($value);
    }

    /**
     * {@inheritDoc}
     */
    public function processQuery(
        string $name,
        mixed $value,
        string $style
    ): array {
        if ($style === 'form' && is_array($value)) {
            return $value;
        }
        return [$name => $this->toString($value)];
    }

    /**
     * {@inheritDoc}
     */
    public function toHeaderValue($value): string
    {
        if ($value instanceof ModelInterface) {
            return self::jsonEncode(
                $this->sanitizeForSerialization($value)
            );
        }
        /**
         * @see https://github.com/vimeo/psalm/issues/9705
         * @psalm-suppress PossiblyInvalidArgument
         */
        return $this->toString($value);
    }

    /**
     * {@inheritDoc}
     */
    public function fileToFormValue($file): array
    {
        $paramFiles = is_array($file) ? $file : [$file];
        $return = [];
        foreach ($paramFiles as $paramFile) {
            $return[] = GuzzleUtils::tryFopen(
                $this->toFormValue($paramFile),
                'rb'
            );
        }
        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function toFormValue($value): string
    {
        if ($value instanceof \SplFileObject) {
            $path = $value->getRealPath();
            if ($path === false) {
                throw new ApiSerializationException('Unable to find file.');
            }
            return $path;
        }
        if ($value instanceof ModelInterface) {
            return self::jsonEncode(
                $this->sanitizeForSerialization($value)
            );
        }
        /**
         * @see https://github.com/vimeo/psalm/issues/9705
         * @psalm-suppress PossiblyInvalidArgument
         */
        return $this->toString($value);
    }

    /**
     * {@inheritDoc}
     */
    public function toString($value): string
    {
        if ($value instanceof \DateTime) {
            return $value->format($this->dateTimeFormat);
        } elseif (is_bool($value)) {
            return $value ? 'true' : 'false';
        }
        return (string)$value;
    }

    /**
     * {@inheritDoc}
     */
    public function toBodyValue(mixed $object, string $contentType)
    {
        if ($contentType === 'application/json') {
            return self::jsonEncode(
                $this->sanitizeForSerialization($object)
            );
        } elseif ($object instanceof StreamInterface) {
            return $object;
        } elseif (!is_array($object)) {
            return (string)$object;
        }
        return '';
    }

    /**
     * {@inheritDoc}
     */
    public function serializeCollection(
        $collection,
        string $style,
        bool $allowCollectionFormatMulti = false
    ): string {
        if (is_scalar($collection)) {
            // Ensure the return is a string.
            /** @psalm-suppress RedundantCastGivenDocblockType */
            return (string) $collection;
        }
        if ($allowCollectionFormatMulti && ('multi' === $style)) {
            // http_build_query() almost does the job for us. We just
            // need to fix the result of multidimensional arrays.
            $string = preg_replace(
                '/%5B[0-9]+%5D=/',
                '=',
                http_build_query($collection, '', '&')
            );
            if ($string === null) {
                throw new ApiSerializationException(
                    'Unable to serialize collection'
                );
            }
            return $string;
        }
        return match ($style) {
            'pipeDelimited', 'pipes' => implode('|', $collection),
            'tsv' => implode("\t", $collection),
            'spaceDelimited', 'ssv' => implode(' ', $collection),
            default => implode(',', $collection),
        };
    }

    /**
     * Helper to decode json and throw exception if something goes wrong.
     *
     * @param mixed $data
     *   Some data to encode with json.
     *
     * @return string
     *   An encoded json string.
     */
    private static function jsonEncode(mixed $data): string
    {
        $json = json_encode($data);
        if ($json === false) {
            throw new \InvalidArgumentException('json encoding failure: ' . json_last_error_msg());
        }
        return $json;
    }
}
