<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Serialization;

use Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException;
use Neclimdul\OpenapiPhp\Helper\HeaderSelector;
use Neclimdul\OpenapiPhp\Helper\Model\AdditionalPropertiesInterface;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Psr\Http\Message\StreamInterface;

class Deserializer implements DeserializerInterface
{
    private const PRIMITIVES = [
        'bool',
        'boolean',
        'int',
        'integer',
        'double',
        'float',
        'object',
        'array',
        'string',
        'null',
    ];

    /**
     * {@inheritDoc}
     * @template T
     * @phpstan-return ($data is null ? null : ($type is class-string<T> ? T : scalar))
     */
    public function deserialize(
        mixed $data,
        string $type,
        array $httpHeaders = ['Content-Type' => ['application/json']]
    ): mixed {
        if (null === $data) {
            return null;
        }

        // Trim to support FQDN and class constant.
        if (ltrim($type, '\\') === \SplFileObject::class) {
            /** @var \Psr\Http\Message\StreamInterface $data */
            /** @var T $r */
            $r = new \SplFileObject('php://temp', 'r+');
            $data->rewind();
            while (!$data->eof()) {
                $r->fwrite($data->read(8192));
            }
            return $r;
        }

        // @todo support more content types.
        $decoded = $data instanceof StreamInterface ? (string)$data : $data;
        if (!empty($decoded) && is_string($decoded)) {
            $ct = (new HeaderSelector())->selectContentHeader($httpHeaders['Content-Type'] ?? ['application/json']);
            switch ($ct) {
                case 'application/xml':
                    $x = libxml_use_internal_errors(true);
                    $decoded = simplexml_load_string((string) $data);
                    libxml_use_internal_errors($x);
                    if ($decoded === false) {
                        // libxml_get_errors?
                        throw new ApiSerializationException('Error decoding json: ' . $data, 0);
                    }
                    break;
                case 'application/json':
                default:
                    try {
                        $decoded = json_decode((string) $data, null, 512, JSON_THROW_ON_ERROR);
                    } catch (\JsonException $e) {
                        throw new ApiSerializationException('Error decoding json: ' . $data, 0, $e);
                    }
            }
        }
        return $decoded !== null ? $this->doDeserialize($decoded, $type) : null;
    }

    /**
     * Deserialize a JSON string into an object.
     *
     * @param mixed $data
     *   Object or primitive to be deserialized.
     * @param string $type
     *   Class name is passed as a string.
     *
     * @return mixed
     *   A single or an array of $type instances.
     */
    public function doDeserialize(mixed $data, string $type): mixed
    {
        if (str_ends_with($type, '[]')) {
            if (!is_array($data)) {
                throw new \InvalidArgumentException("Invalid array '$type'");
            }

            /** @var class-string $subType */
            $subType = substr($type, 0, -2);
            $values = [];
            foreach ($data as $value) {
                $values[] = $this->doDeserialize($value, $subType);
            }
            return $values;
        }

        // for associative array e.g. array<string,int>
        if (preg_match('/^(?:array<|map\[)(.*)[>\]]$/', $type, $matches)) {
            settype($data, 'array');
            $inner = $matches[1];
            $deserialized = [];
            if (strrpos($inner, ',') !== false) {
                $subType_array = explode(',', $inner, 2);
                /** @psalm-suppress PossiblyUndefinedArrayOffset */
                $subType = $subType_array[1];
                foreach ($data as $key => $value) {
                    $deserialized[$key] = $this->doDeserialize(
                        $value,
                        $subType
                    );
                }
            }
            return $deserialized;
        }

        if ($type === '\DateTime') {
            // Some APIs return an invalid, empty string as a
            // date-time property. DateTime::__construct() will return
            // the current time for empty input which is probably not
            // what is meant. The invalid empty string is probably to
            // be interpreted as a missing field/value. Let's handle
            // this graceful.
            if (!empty($data)) {
                return new \DateTime($data);
            } else {
                return null;
            }
        }

        /** @psalm-suppress ParadoxicalCondition */
        if (in_array($type, self::PRIMITIVES, true)) {
            settype($data, $type);
            return $data;
        }

        // If type is mixed, it's not a model so just return it. We don't know
        // what else to do.
        if ($type === 'mixed') {
            return $data;
        }

        // We couldn't do a scalar cast, so we know this is class string.
        /** @psalm-suppress ArgumentTypeCoercion */
        if (method_exists($type, 'getAllowableEnumValues')) {
            if (!in_array($data, $type::getAllowableEnumValues(), true)) {
                $imploded = implode("', '", $type::getAllowableEnumValues());
                throw new \InvalidArgumentException(
                    "Invalid value for enum '$type', must be one of: '$imploded'"
                );
            }
            return $data;
        } else {
            if (is_subclass_of($type, ModelInterface::class)) {
                // If a discriminator is defined and points to a valid subclass, use it.
                /** @var string|null $discriminator */
                $discriminator = $type::DISCRIMINATOR ?? null;
                /** @psalm-suppress RiskyTruthyFalsyComparison */
                if (
                    !empty($discriminator)
                    && isset($data->{$discriminator})
                    && is_string($data->{$discriminator})
                ) {
                    $reflection_class = new \ReflectionClass($type);
                    $namespace = $reflection_class->getNamespaceName();
                    $subclass = $namespace . '\\' . $data->{$discriminator};
                    if (is_subclass_of($subclass, $type)) {
                        $type = $subclass;
                    }
                }
            }
        }

        if (!is_object($data)) {
            throw new \InvalidArgumentException("Invalid object for '$type'");
        }

        /** @psalm-suppress MixedMethodCall */
        $instance = new $type();
        if ($instance instanceof ModelInterface) {
            $attribute_map = $instance::attributeMap();
            $setters = $instance::setters();
            foreach ($instance::openAPITypes() as $property => $model_types) {
                $data_prop = $attribute_map[$property];
                if (isset($data->$data_prop)) {
                    $propertySetter = $setters[$property];
                    $instance->$propertySetter(
                        $this->doDeserialize(
                            $data->$data_prop,
                            $model_types
                        )
                    );
                }
            }
            // Add any additional properties directly. This doesn't get any
            // fancy serialization which is unfortunate, but it gives us basic
            // access to additional properties.
            if ($instance instanceof AdditionalPropertiesInterface) {
                $additional = array_diff_key(get_object_vars($data), array_flip($attribute_map));
                foreach ($additional as $property => $value) {
                    $instance->setAdditionalProperty($property, $value);
                }
            }
        }
        return $instance;
    }
}
