<?php

declare(strict_types=1);

namespace Neclimdul\OpenapiPhp\Helper\Serialization;

/**
 * Deserialize response data.
 */
interface DeserializerInterface
{
    /**
     * Deserialize a JSON string into an object.
     *
     * @template T
     * @param \Stringable|string|null $data
     *   Object or primitive to be deserialized.
     * @param string $type
     *   Some php type that be returned.
     * @param array<array-key, string[]> $httpHeaders
     *   A list of headers from the response.
     * @phpstan-param class-string<T>|string $type
     *
     * @return mixed
     *   A single or an array of $type instances.
     * @phpstan-return ($data is null ? null : ($type is class-string<T> ? T : scalar))
     * @throws \Neclimdul\OpenapiPhp\Helper\Exception\ApiSerializationException
     */
    public function deserialize(
        mixed $data,
        string $type,
        array $httpHeaders = ['Content-Type' => ['application/json']],
    ): mixed;
}
