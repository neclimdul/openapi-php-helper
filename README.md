# OpenAPI PHP Templates
PHP OpenAPI helpers.

This project aims to consolidate shared logic for PHP OpenAPI projects. This
provides a more complete solution with less shared boilerplate.

## Goals
This project shares its goals with its sibling template project it is designed
to support.

  - Faster development
  - Bug fixes
  - Additional spec compatibility
  - Testing

## Usage
Generally this package will be used with the templates it is designed to
support and not on its own. If you need to add it to your project you can do
so via composer.

```shell
composer require neclimdul/openapi-php-helper
```

## License

This library is derived from the OpenAPI and Swagger projects and is released under the [Apache 2.0](LICENSE).
